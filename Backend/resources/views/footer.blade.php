<footer class="footer pb-4">
    <div class="container">
        <nav class="row text-center justify-content-center">
            <ul class="list-inline d-flex">
                <li class="mx-4">
                    <a class="text-white" href="https://api.whatsapp.com/send?phone=50376444432" target="_blank">
                        <i class="fa-2x fab fa-whatsapp-square text-success"></i>
                    </a>
                </li>
                <li class="mx-4">
                    <a class="text-white" href="https://www.messenger.com/t/websis.me" target="_blank">
                        <i class="fa-2x fab fa-facebook-messenger text-info"></i>
                    </a>
                </li>
                <li class="mx-4">
                    <a class="text-white" href="mailto:info@websis.me" target="_blank">
                        <i class="fa-2x fa fa-envelope-square text-danger"></i>
                    </a>
                </li>
            </ul>
        </nav>

        <div class="copyright">
            Hecho en El Salvador con <i class="material-icons text-danger">favorite</i> por 
            <a href="http://websis.me" target="_blank">Websis</a>
            <br>&copy; {{ date('Y') }}
        </div>
    </div>
</footer>