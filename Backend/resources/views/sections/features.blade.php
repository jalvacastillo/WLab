<div class="container">
	<div class="features">
	  <div class="row text-center">
	    <div class="col-md-4">
	      <div class="info pt-4">
	        <div class="icon icon-warning">
	          <i class="material-icons">access_time</i>
	        </div>
	        <h4 class="info-title mt-2">Rápido</h4>
	        <p>Diseñado para operar de forma rápida y ahorrar tiempo.</p>
	      </div>
	    </div>
	    <div class="col-md-4">
	      <div class="info pt-4">
	        <div class="icon icon-success">
	          <i class="material-icons">verified_user</i>
	        </div>
	        <h4 class="info-title mt-2">Seguro</h4>
	        <p>Estamos constantemente actualizándolo y adaptándolo a las nuevas necesidades.</p>
	      </div>
	    </div>
	    <div class="col-md-4">
	      <div class="info pt-4">
	        <div class="icon icon-danger">
	          <i class="material-icons">assistant_photo</i>
	        </div>
	        <h4 class="info-title mt-2">Soporte</h4>
	        <p>Brindamos capacitaciones y asesorías en la implementación y ejecución.</p>
	      </div>
	    </div>
	  </div>
	</div>
</div>