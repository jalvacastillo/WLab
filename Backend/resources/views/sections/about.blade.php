<div class="section text-center pb-0">
  <div class="container">
    <div class="row">
      <div class="col-md-8 ml-auto mr-auto">
        <h2 itemprop="name" class="title">
            ¿Qué es Wclinic?
        </h2>
        <h5 itemprop="description" class="description">
            Wclinic te permite gestionar tu clínicas médicas sin importar donde estés.
            <br> <br>
            Monitorea y automatiza los procesos de facturación y la gestión de inventarios, compras, clientes, proveedores y más.
        </h5>
      </div>
    </div>
    </div>
</div>