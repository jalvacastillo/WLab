<div class="page-header header-filter clear-filter" data-parallax="true" style="background-image: url({{asset('img/bg1.jpeg')}}); transform: translate3d(0px, 0px, 0px);">
	<div class="container">
		<div class="row">
			<div class="col-md-9 ml-auto mr-auto">
				<div class="brand mx-1">
					<h1 itemprop="name">Wclinic</h1>
					<h3>Gestiona tu clínica médica desde cualquier lugar.</h3>
					<div class="col-xs-12 text-center">
						<a href="{{ route('registro') }}" class="btn btn-primary btn-lg color-filter">Pruébalo gratis</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>