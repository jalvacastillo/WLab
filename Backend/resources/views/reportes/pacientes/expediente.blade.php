<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Expediente</title>
</head>

<style>
    .text-center { text-align: center; }
    .text-right { text-align: right; }
    .text-left { text-align: left; }
    *{ font-family: sans-serif; color: #333; }
    @page { margin: 100px 100px; }
    .header { position: fixed; top: -100px; opacity: .6;  }
    .footer{ position: fixed; bottom: -10px; opacity: .6; }
    .bg{ width: 840px; position: fixed; top: -150px; left: -120px; opacity: .5; z-index: -1;}
    .table {width: 100%; border-collapse: collapse; margin: auto;}
    .table-bordered td, .table-bordered th, .table-bordered td, {border: 0.5px solid gray; padding: 5px 10px; }
    hr{ border: 0.5px solid #D1D0D0; }
    .notas>br:before {content: "*"; color: black; }
    p{ text-align: justify; }
    .badge{ background-color: #1B5FFA; color: white; padding: 5px;}
    .completado{text-decoration:line-through; color: gray;}

</style>

<body>
    {{-- <img class="bg" src="imgs/bg.jpg"> --}}
    <div class="header text-center">
        <img src="img/logo.jpg" alt="Logo" width="100">
    </div>
    <div class="footer">
        <hr>
        <h4 class="text-center">
            {{ $paciente->empresa->nombre }} | {{ $paciente->empresa->correo }} | {{ $paciente->empresa->telefono }}
        </h4>
    </div>

    <h2 class="text-center">EXPEDIENTE</h2>
    <table class="table">
        <tr>
            <td>
                <img src="img/{{ $paciente->img }}" alt="Logo" width="100">
            </td>
            <td class="text-right">
                <b>{{ $paciente->empresa->nombre }}</b><br>
                {{ $paciente->empresa->propietario }}<br>
                {{ $paciente->fecha->format('d/m/Y') }}
            </td>
        </tr>
    </table>

    <table class="table">
        <tbody>
            <tr>
                <td>
                    <p class="text-left">
                        <b>Paciente:</b> <br>
                        <b>Nombre</b>: {{ $paciente->nombre }}<br>
                        <b>Edad</b>: {{ $paciente->fecha_nacimiento->format('d/m/Y') }}<br>
                        <b>Edad</b>: {{ $paciente->edad }}<br>
                        <b>DUI</b>: {{ $paciente->dui }}<br>
                    </p>
                </td>
                <td>
                    <p class="text-left">
                        <b>Dirección:</b> <br>
                        <b>Municipío</b>: {{ $paciente->municipio }}<br>
                        <b>Departamento</b>: {{ $paciente->departamento }}<br>
                        <b>Dirección</b>: {{ $paciente->direccion }}
                    </p>
                </td>
            </tr>
        </tbody>
        
    </table>

    <table class="table table-bordered"  style="margin-top: 20px;">
        <tbody>
            <tr> <td>Citología:</td> <td>{{ $paciente->citologia }}</td> </tr>
            <tr> <td>Responsable:</td> <td>{{ $paciente->responsable }}</td> </tr>
            <tr> <td>Madre:</td> <td>{{ $paciente->madre }}</td> </tr>
            <tr> <td>Padre:</td> <td>{{ $paciente->padre }}</td> </tr>
            <tr> <td>Alergias:</td> <td>{{ $paciente->alergias }}</td> </tr>
            <tr> <td>Antecedentes:</td> <td>{{ $paciente->antecedentes }}</td> </tr>
            <tr> <td>Medicación actual:</td> <td>{{ $paciente->medicacion_actual }}</td> </tr>
        </tbody>
    </table>


    <p class="text-center" style="margin-top: 200px;">
        _________________________________________
        <br>
        <b>Firma del médico:</b>
    </p>

    

</body>
</html>
