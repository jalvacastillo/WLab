<?php

return [

    'about_title' => "Hi, I'm Diana Ramírez",
    'about_content' => "Welcome to my store, I am a single mother of three beautiful girls and I will share my story with you.
                <br/><br/>
                I became a single mother at the age of 34 and had no job or experience in any professional area. This lead me to study and take courses in accounting, administration and nursing.
                <br/><br/>
                After studying for a while, I was given the opportunity to work as a nursing assistant which was very exhausting and took a toll on my health so I had to leave to prioritize my health. I had more opportunities but due to unforeseen circumstances, I realized there needed to be a radical change in my life.
                <br/><br/>
                A day came that marked me for life and since then I decided to start my own business. I did not know how to go about opening a business so I did a lot of research. I also requested help from the Los Angeles municipality, who supported me by answering all my doubts, concerns and were able to guide me to undertake my dream.
                <br/><br/>
                Now I am starting the biggest adventure of my life. This project fills me with uncertainty but I am very optimistic this will be my greatest success!
                <br/><br/>
                Nothing has been easy but I believe with hard work everything is possible. I am fortunate that I can depend on my family and friends who fully support and believe this will be my greatest accomplishment. My dream is to grow so much so that I can provide work for other single mothers who, like me, have had to get ahead in life alone.",

];
