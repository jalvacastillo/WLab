<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacientesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacientes', function(Blueprint $table)
        {
            $table->increments('id');

            $table->date('fecha');
            $table->string('nombre');
            $table->string('dui')->nullable();
            $table->string('nit')->nullable();
            $table->date('fecha_nacimiento')->nullable();
            $table->string('direccion')->nullable();
            $table->string('municipio')->nullable();
            $table->string('departamento')->nullable();
            $table->string('telefono')->nullable();
            $table->string('correo')->nullable();
            $table->string('sexo')->nullable();
            $table->string('profesion')->nullable();
            $table->string('estado_civil')->nullable();
            $table->string('citologia')->nullable();
            $table->string('responsable')->nullable();
            $table->string('madre')->nullable();
            $table->string('padre')->nullable();
            $table->string('alergias')->nullable();
            $table->string('antecedentes')->nullable();
            $table->string('medicacion_actual')->nullable();
            $table->string('etiquetas')->nullable();
            $table->string('img')->nullable();
            $table->text('nota')->nullable();
            $table->integer('empresa_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pacientes');
    }

}
