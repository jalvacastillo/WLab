<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCocinaDepartamentoDetallesTable extends Migration
{

    public function up()
    {
        Schema::create('cocina_departamento_detalles',function($table) {
            $table->increments('id');

            $table->integer('categoria_id');
            $table->integer('departamento_id')->unsigned();
            
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::drop('cocina_departamento_detalles');
    }
}
