<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditoFiadoresTable extends Migration {

    public function up()
    {
        Schema::create('credito_fiadores', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('credito_id');
            $table->integer('cliente_id');
            $table->text('nota');


            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('credito_fiadores');
    }

}
