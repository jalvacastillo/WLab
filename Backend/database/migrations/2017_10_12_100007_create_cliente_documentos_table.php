<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClienteDocumentosTable extends Migration {

    public function up()
    {
        Schema::create('cliente_documentos', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('nombre');
            $table->string('url');
            $table->text('nota')->nullable();
            $table->integer('cliente_id');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('cliente_documentos');
    }

}
