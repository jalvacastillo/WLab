<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamenesQuimicasDetalleTable extends Migration {

	public function up()
	{
		Schema::create('examenes_quimicas_detalle', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('quimica_id')->nullable();
			$table->string('resultado', 150)->nullable();
			$table->string('examen', 150)->nullable();
			$table->string('valor', 150)->nullable();
			
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('examenes_quimicas_detalle');
	}

}
