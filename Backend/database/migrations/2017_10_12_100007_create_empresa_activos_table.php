<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaActivosTable extends Migration
{

    public function up()
    {
        Schema::create('empresa_activos', function (Blueprint $table) {
            $table->increments('id');
            
            $table->date('fecha_compra');
            $table->string('nombre');
            $table->text('descripcion');
            $table->string('ubicacion');
            $table->decimal('costo', 9,2);
            $table->integer('empresa_id');

            $table->timestamps();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('empresa_activos');
    }
}
