<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductoComposicionesTable extends Migration {

    public function up()
    {
        Schema::create('producto_composiciones', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('producto_id');
            $table->integer('compuesto_id');
            $table->string('medida');
            $table->decimal('cantidad', 9,2);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('producto_composiciones');
    }

}
