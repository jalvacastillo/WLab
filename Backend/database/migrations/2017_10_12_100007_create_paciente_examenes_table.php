<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacienteExamenesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paciente_examenes', function(Blueprint $table)
        {
            $table->increments('id');

            $table->date('fecha');
            $table->integer('paciente_id');
            $table->string('examen_id');
            $table->string('medico')->nullable();
            $table->string('estado');
            $table->text('nota')->nullable();
            $table->integer('usuario_id');
            $table->integer('consulta_id')->nullable();
            $table->integer('sucursal_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('paciente_examenes');
    }

}
