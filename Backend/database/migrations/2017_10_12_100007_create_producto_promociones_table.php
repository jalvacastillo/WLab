<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductoPromocionesTable extends Migration {

    public function up()
    {
        Schema::create('producto_promociones', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('producto_id');            
            $table->decimal('precio', 9,2);
            $table->datetime('inicio');
            $table->datetime('fin');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('producto_promociones');
    }

}
