<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamenesHecesTable extends Migration {

	public function up()
	{
		Schema::create('examenes_heces', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('color', 100)->nullable();
			$table->string('consistencia', 100)->nullable();
			$table->string('sangre', 100)->nullable();
			$table->string('restos', 100)->nullable();
			$table->string('entrocitos', 100)->nullable();
			$table->string('levadura', 100)->nullable();
			$table->string('mucus', 100)->nullable();
			$table->string('leucocitos', 100)->nullable();
			$table->string('flora', 100)->nullable();
			$table->string('activos', 100)->nullable();
			$table->string('quistes', 100)->nullable();
			$table->string('larvas', 100)->nullable();
			$table->string('huevos', 100)->nullable();			
			$table->text('observaciones')->nullable();
			
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('examenes_heces');
	}

}
