<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacienteDocumentosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paciente_documentos', function(Blueprint $table)
        {
            $table->increments('id');

            $table->date('fecha');
            $table->string('nombre');
            $table->string('url');
            $table->integer('paciente_id');
            $table->integer('usuario_id');


            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('paciente_documentos');
    }

}
