<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadosContratosTable extends Migration
{

    public function up()
    {
        Schema::create('empleados_contratos', function (Blueprint $table) {
            $table->increments('id');
            
            $table->date('fecha');
            $table->integer('dias');
            $table->integer('horas');
            $table->decimal('sueldo', 6,2);
            $table->boolean('renta')->default(0);
            $table->boolean('afp')->default(0);
            $table->boolean('isss')->default(0);
            $table->integer('empleado_id');

            $table->timestamps();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('empleados_contratos');
    }
}
