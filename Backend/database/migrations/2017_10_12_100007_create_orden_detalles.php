<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdenDetalles extends Migration
{

    public function up()
    {
        Schema::create('orden_detalles',function($table) {
            $table->increments('id');

            $table->integer('producto_id')->unsigned();
            $table->string('estado');
            $table->integer('cantidad');
            $table->decimal('precio',6,2);
            $table->decimal('costo',6,2);
            $table->decimal('descuento',6,2);
            $table->decimal('total',6,2);
            $table->text('nota')->nullable();
            $table->integer('tiempo')->nullable();
            $table->integer('orden_id')->unsigned();
            
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::drop('orden_detalles');
    }
}
