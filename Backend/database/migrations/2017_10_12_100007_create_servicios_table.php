<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiciosTable extends Migration {

	public function up()
	{
		Schema::create('servicios',function($table) {
            $table->increments('id');

            $table->string('nombre');
            $table->decimal('precio',6,2);
            $table->text('descripcion')->nullable();
            $table->string('tags')->nullable();
            $table->integer('empresa_id')->unsigned();
            
            $table->timestamps();
        });
	}

	public function down()
	{
		Schema::drop('servicios');
	}

}
