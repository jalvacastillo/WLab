    <?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresasPlanillasTable extends Migration {

    public function up()
    {
        Schema::create('empresa_planillas', function(Blueprint $table)
        {
            $table->increments('id');

            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->decimal('total',6,2 );
            $table->text('nota')->nullable();
            $table->integer('usuario_id');
            $table->integer('empresa_id');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('empresa_planillas');
    }

}
