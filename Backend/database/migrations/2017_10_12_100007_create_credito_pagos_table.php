<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditoPagosTable extends Migration {

    public function up()
    {
        Schema::create('credito_pagos', function(Blueprint $table)
        {
            $table->increments('id');

            $table->date('fecha');
            $table->integer('credito_id');
            $table->string('metodo_pago');
            $table->decimal('saldo_inicial', 9,2);
            $table->decimal('cuota', 9,2);
            $table->decimal('interes', 9,2);
            $table->decimal('mora', 9,2)->default(0);
            $table->decimal('descuento', 9,2)->default(0);
            $table->decimal('comision', 9,2)->default(0);
            $table->decimal('seguro', 9,2)->default(0);
            $table->decimal('abono', 9,2);
            $table->decimal('saldo_final', 9,2);
            $table->integer('usuario_id');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('credito_pagos');
    }

}
