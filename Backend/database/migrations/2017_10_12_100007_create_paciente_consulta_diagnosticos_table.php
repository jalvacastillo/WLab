<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacienteConsultaDiagnosticosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paciente_consultas_diagnosticos', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('codigo');
            $table->text('descripcion');
            $table->integer('consulta_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('paciente_consultas_diagnosticos');
    }

}
