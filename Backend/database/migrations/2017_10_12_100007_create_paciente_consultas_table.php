<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacienteConsultasTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paciente_consultas', function(Blueprint $table)
        {
            $table->increments('id');

            $table->datetime('fecha');
            $table->integer('paciente_id');
            $table->string('tipo');
            $table->string('estado');
            $table->string('motivo');
            $table->longText('consulta')->nullable();
            $table->longText('diagnostico')->nullable();
            $table->longText('receta')->nullable();
            $table->longText('examen')->nullable();
            $table->integer('usuario_id');
            $table->integer('sucursal_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('paciente_consultas');
    }

}
