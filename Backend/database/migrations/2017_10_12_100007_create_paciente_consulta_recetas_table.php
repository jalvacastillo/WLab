<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacienteConsultaRecetasTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paciente_consulta_recetas', function(Blueprint $table)
        {
            $table->increments('id');


            $table->integer('producto_id');
            $table->text('dosis')->nullable();
            $table->integer('cantidad');
            $table->integer('consulta_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('paciente_consulta_recetas');
    }

}
