<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditosTable extends Migration {

    public function up()
    {
        Schema::create('creditos', function(Blueprint $table)
        {
            $table->increments('id');

            $table->date('fecha');
            $table->integer('venta_id');
            $table->integer('cliente_id');
            $table->decimal('total', 9,2);
            $table->decimal('interes', 9,2);
            $table->integer('plazo');
            $table->decimal('prima', 9,2);
            $table->text('nota')->nullable();
            $table->integer('usuario_id');
            $table->integer('empresa_id');


            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('creditos');
    }

}
