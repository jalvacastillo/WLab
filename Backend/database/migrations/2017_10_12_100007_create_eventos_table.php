<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventosTable extends Migration {

	public function up()
	{
		Schema::create('eventos',function($table) {
            $table->increments('id');

            $table->date('fecha');
            $table->string('estado');
            $table->string('categoria', 50);
            $table->string('lugar',100);
            $table->decimal('pagado', 6,2);
            $table->decimal('total', 6,2);
            $table->text('nota');
            $table->integer('cliente_id')->unsigned();
            $table->integer('usuario_id')->unsigned();
            $table->integer('sucursal_id')->unsigned();
            
            $table->timestamps();
        });
	}

	public function down()
	{
		Schema::drop('eventos');
	}

}
