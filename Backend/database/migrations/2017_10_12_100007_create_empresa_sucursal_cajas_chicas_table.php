<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaSucursalCajasChicasTable extends Migration {

    public function up()
    {
        Schema::create('empresa_sucursal_cajas_chicas', function(Blueprint $table)
        {
            $table->increments('id');

            $table->date('apertura')->nullable();
            $table->date('cierre')->nullable();
            $table->decimal('entradas', 9,2)->default(0);
            $table->decimal('salidas', 9,2)->default(0);
            $table->decimal('saldo', 9,2)->default(0);
            $table->text('descripcion')->nullable();
            $table->integer('usuario_id');
            $table->integer('sucursal_id');
            $table->timestamps();

        });
    }

    public function down()
    {
        Schema::drop('empresa_sucursal_cajas_chicas');
    }

}
