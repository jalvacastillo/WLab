<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration {

	public function up()
	{
		Schema::create('productos', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('nombre');
			$table->text('descripcion')->nullable();
			$table->string('codigo')->unique()->nullable();
			$table->string('medida')->default('Unidad');
			$table->decimal('precio', 9,2)->default(0);
			$table->decimal('precio2', 9,2)->nullable();
			$table->decimal('precio3', 9,2)->nullable();
			$table->decimal('precio4', 9,2)->nullable();

			$table->decimal('costo', 9,2)->default(0);
			$table->decimal('costo_anterior', 9,2)->default(0);
			$table->integer('categoria_id')->nullable();
			$table->string('tipo')->nullable();
			$table->string('tipo_impuesto')->default('Gravada');
			$table->boolean('compuesto')->default(0);
			$table->boolean('activo')->default(1);
			$table->text('nota')->nullable();
			$table->integer('empresa_id');

			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('productos');
	}

}
