    <?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresasTable extends Migration {

    public function up()
    {
        Schema::create('empresas', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('nombre');
            $table->string('propietario')->nullable();
            $table->string('sector')->nullable();
            $table->string('giro')->nullable();
            $table->string('nit')->nullable();
            $table->string('ncr')->nullable();
            $table->string('tamano')->nullable();
            $table->string('telefono')->nullable();
            $table->string('correo')->nullable();
            $table->string('logo')->default('empresas/default.jpg');
            $table->string('valor_inventario')->default('Promedio');
            $table->string('ips', 255)->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('empresas');
    }

}
