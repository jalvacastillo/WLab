<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamenesOrinasTable extends Migration {

	public function up()
	{
		Schema::create('examenes_orinas', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('color',100)->nullable();
			$table->string('aspecto',100)->nullable();
			$table->string('densidad',100)->nullable();
			$table->string('esterasa',100)->nullable();
			$table->string('nitritos',100)->nullable();
			$table->string('reaccion',100)->nullable();
			$table->string('proteinas',100)->nullable();
			$table->string('glucosa',100)->nullable();
			$table->string('cetonicos',100)->nullable();
			$table->string('urobitmogeno',100)->nullable();
			$table->string('bilirubina',100)->nullable();
			$table->string('sangre',100)->nullable();
			$table->string('bacterias',100)->nullable();
			$table->string('leucocitos',100)->nullable();
			$table->string('hematies',100)->nullable();
			$table->string('cilindros',100)->nullable();
			$table->string('cristales',100)->nullable();
			$table->string('celulas',100)->nullable();
			$table->text('otros')->nullable();

			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('examenes_orinas');
	}

}
