<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacienteCitasTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paciente_citas', function(Blueprint $table)
        {
            $table->increments('id');

            $table->date('fecha');
            $table->string('tipo');
            $table->string('estado');
            $table->string('frecuencia')->nullable();
            $table->text('nota');
            $table->integer('paciente_id');
            $table->integer('medico_id')->nullable();
            $table->integer('usuario_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('paciente_citas');
    }

}
