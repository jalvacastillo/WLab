<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaSucursalBodegasTable extends Migration {

    public function up()
    {
        Schema::create('empresa_sucursal_bodegas', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('nombre');
            $table->string('descripcion')->nullable();
            $table->integer('sucursal_id');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('empresa_sucursal_bodegas');
    }

}
