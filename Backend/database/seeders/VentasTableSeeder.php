<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Ventas\Venta;
use App\Models\Ventas\Detalle;
     
class VentasTableSeeder extends Seeder {
     
    public function run()
    {
        $faker = \Faker\Factory::create();

        for($i = 1; $i <= 20 ; $i++)
        {
            $table = new Venta;

            $table->fecha           = $faker->dateTimeBetween($startDate = '-10 days', $endDate = 'now', $timezone = null);
            $table->correlativo     = $i;
            $table->estado          = 'Cobrada';
            $table->metodo_pago     = $faker->randomElement(['Efectivo', 'Tarjeta']);
            $table->tipo_documento  = $faker->randomElement(['Factura', 'Crédito Fiscal', 'Ticket']);
            $table->referencia      = $faker->numberBetween(10000,50000);
            $table->iva_retenido    =  0;  
            $table->iva             = $faker->numberBetween(1,50);
            $table->subcosto        = $faker->numberBetween(50,150);
            $table->subtotal        = $faker->numberBetween(50,150);
            $table->nota            = $i;
            $table->total           = $faker->numberBetween(100,250);
            $table->caja_id         = 1;
            $table->corte_id        = 1;
            $table->cliente_id      = $faker->numberBetween(1,20);
            $table->usuario_id      = $faker->numberBetween(4,6);
            $table->sucursal_id     = $faker->numberBetween(1,2);
            $table->save();

            for($j = 1; $j <= $faker->numberBetween(1,5) ; $j++)
            {
                $table = new Detalle;

                $table->producto_id = $faker->numberBetween(1,100);
                $table->cantidad    = $faker->numberBetween(1,20);
                $table->precio      = $faker->numberBetween(1,20);
                $table->costo      = $faker->numberBetween(1,20);
                $table->descuento   = 0;
                $table->tipo_impuesto =  'Gravada';  
                $table->subcosto      = $table->costo * $table->cantidad;  
                $table->total         = $table->precio * $table->cantidad;  
                $table->subtotal      = $table->total / 1.13;  
                $table->iva           = $table->subtotal * 0.13;
                
                $table->venta_id    = $i;
                $table->save();
                
            }
            
        }
    }
     
}
