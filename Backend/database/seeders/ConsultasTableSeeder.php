<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Pacientes\Consultas\Consulta;
use App\Models\Pacientes\Consultas\Signo;
use App\Models\Pacientes\Consultas\Receta;
use App\Models\Pacientes\Examen;
     
class ConsultasTableSeeder extends Seeder {
     
    public function run()
    {
        $faker = \Faker\Factory::create();

        for($i = 0; $i < 50 ; $i++)
        {
                $table = new Consulta;


                $table->fecha           = $faker->dateTimeBetween($startDate = '-10 days', $endDate = 'now', $timezone = null);
                $table->paciente_id     = $faker->numberBetween(1,50); 
                $table->tipo            = $faker->randomElement(['En Clínica', 'Por Teléfono', 'A Domicilio', 'Video Consulta']);
                $table->estado          = $faker->randomElement(['En Espera', 'En Proceso', 'Finalizada', 'Cancelada']);
                $table->motivo          = $faker->name;
                $table->consulta        = $faker->text;
                $table->diagnostico     = $faker->text;
                $table->receta          = $faker->text;
                $table->examen          = $faker->text;
                $table->usuario_id      = 1;
                $table->sucursal_id     = 1;
                $table->save();

                $table2 = new Signo;
                $table2->fecha          = $table->fecha;
                $table2->peso           = $faker->numberBetween(100,200);;
                $table2->peso_unidad    = 'Lb';
                $table2->estatura       = $faker->numberBetween(100,200);;
                $table2->estatura_unidad = 'Cm';
                $table2->temperatura    = $faker->numberBetween(50,60);;
                $table2->temperatura_unidad     = 'Oral';
                $table2->presion        = $faker->numberBetween(100,200);;
                $table2->nota           = $table->name;
                $table2->consulta_id    = $i + 1;
                $table2->usuario_id     = 1;
                $table2->save();

                $table3 = new Receta;

                $table3->producto_id    = $faker->numberBetween(1,50);
                $table3->dosis          = $faker->text(30);
                $table3->cantidad       = $faker->numberBetween(1,2);
                $table3->consulta_id    = $i + 1;
                $table3->save();

                $table4 = new Examen;
                $table4->fecha = $table->fecha;
                $table4->paciente_id = $table->paciente_id;
                $table4->examen_id  = $faker->numberBetween(1,800);
                $table4->medico     = $faker->name;
                $table4->estado     = $faker->randomElement(['Pendiente', 'Entregado']);
                $table4->nota       = $faker->name;
                $table4->usuario_id = 1;
                $table4->consulta_id = $i;
                $table4->sucursal_id = 1;
                $table4->save();
            
        }


    }
     
}