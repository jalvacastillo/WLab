<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Admin\Empresa;
use App\Models\Admin\Sucursal;

class EmpresaTableSeeder extends Seeder {
    
    public function run(){

        $table = new Empresa;

        $table->nombre          = 'Nombre Negocio';
        $table->propietario     = 'Nombre Propietario';
        $table->sector          = 'Sector';
        $table->giro            = 'El giro del negocio';
        $table->nit             = '0000-000000-000-0';
        $table->ncr             = '00000-0';
        $table->tamano          = 'Pequeño';
        $table->telefono        = '0000-0000';
        $table->correo          = 'tu@correo.com';

        $table->save();

        $table = new Sucursal;

        $table->nombre        = 'Sucursal 1';
        $table->telefono      = '0000-0000';
        $table->municipio     = 'Ilobasco';
        $table->departamento  = 'Cabañas';
        $table->direccion     = 'Colonia, calle, # casa';
        $table->empresa_id    = 1;

        $table->save();



    }

}