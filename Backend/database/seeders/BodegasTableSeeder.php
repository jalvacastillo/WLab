<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Admin\Bodega;
     
class BodegasTableSeeder extends Seeder {
     
    public function run()
    {

        Bodega::create([ 
            'nombre' => 'Bodega 1',
            'descripcion' => '',
            'sucursal_id' => 1
        ]);

        Bodega::create([ 
            'nombre' => 'Bodega 2',
            'descripcion' => '',
            'sucursal_id' => 1
        ]);
        

    }
     
}