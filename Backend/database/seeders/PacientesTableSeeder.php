<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Pacientes\Paciente;
     
class PacientesTableSeeder extends Seeder {
     
    public function run()
    {
        $faker = \Faker\Factory::create();

        for($i = 0; $i < 50 ; $i++)
        {
                $table = new Paciente;

                $table->fecha       = $faker->date;
                $table->nombre      = $faker->name;
                $table->dui         = $faker->phoneNumber;
                $table->nit         = $faker->phoneNumber;
                $table->fecha_nacimiento  = $faker->date;  
                $table->direccion   = $faker->address;
                $table->municipio   = $faker->city;
                $table->departamento    = $faker->country;
                $table->telefono    = $faker->phonenumber;
                $table->correo      = $faker->email;
                $table->sexo        = $faker->randomElement(['Hombre', 'Mujer']);
                $table->profesion   = $faker->randomElement(['Uno', 'Dos']);
                $table->estado_civil    = $faker->randomElement(['Casado', 'Soltero']);
                $table->citologia   = $faker->text(50);
                $table->responsable = $faker->name;
                $table->madre       = $faker->name;
                $table->padre       = $faker->name;
                $table->alergias    = $faker->text(50);
                $table->antecedentes    = $faker->text(50);
                $table->medicacion_actual   = $faker->text(50);
                $table->etiquetas   = '';
                $table->nota        = $faker->text;
                $table->empresa_id = 1;

                $table->save();
            
        }


    }
     
}