<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Inventario\Producto as MateriaPrima;
use App\Models\Inventario\Inventario;
     
class MateriasPrimasTableSeeder extends Seeder {

    public function run()
    {

        $faker = \Faker\Factory::create();

        for($i = 0; $i < 50 - 1 ; $i++)
        {
            $table = new MateriaPrima;

            // $table->id              = $materiaprima[$i]['IdInventario'];
            $table->nombre          = $faker->name;
            $table->tipo            = "Materia Prima";
            $table->medida          = "Unidad";
            $table->empresa_id      = 1;
            $table->save();

            if ($table->inventario == 1) {
                $inventario = new Inventario;

                $inventario->bodega_id       = 1;
                $inventario->producto_id     = $table->id;
                $inventario->stock           = 100;
                $inventario->stock_min       = $faker->numberBetween(10, 30);
                $inventario->stock_max       = $faker->numberBetween(10, 30);
                $inventario->save();

                $inventario = new Inventario;

                $inventario->bodega_id       = 2;
                $inventario->producto_id     = $table->id;
                $inventario->stock           = 100;
                $inventario->stock_min       = $faker->numberBetween(10, 30);
                $inventario->stock_max       = $faker->numberBetween(10, 30);
                $inventario->save();
            }

            
        }
        
    }



    public function nombreP($name){

        $porciones = explode(" ", $name);
        $newName = '';

        foreach ($porciones as $index=>$palabra) {
            $palabra = mb_strtolower( $palabra ,'UTF-8' );
            if (strlen($palabra) > 2 || $index == 0) {
                $palabra = ucfirst($palabra);
            }
            $newName = $newName .' '. $palabra;
        }

        return $newName;

    }
     
}