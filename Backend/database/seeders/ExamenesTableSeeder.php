<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Pacientes\Examen;
     
class ExamenesTableSeeder extends Seeder {
     
    public function run()
    {
        $faker = \Faker\Factory::create();

        for($i = 0; $i < 50 ; $i++)
        {
                $table = new Examen;

                $table->fecha       = $faker->dateTimeBetween($startDate = '-10 days', $endDate = 'now', $timezone = null);
                $table->paciente_id = $faker->numberBetween(1,50);
                $table->medico      = $faker->name;
                // $table->tipo_examen = $faker->randomElement(['Orina', 'Heces']);
                $table->estado      = $faker->randomElement(['Pendiente', 'Entregado']);
                $table->examen_id  = $faker->numberBetween(1,50);
                $table->nota        = $faker->text;
                $table->usuario_id  = 1;
                $table->sucursal_id  = 1;

                $table->save();
            
        }


    }
     
}
