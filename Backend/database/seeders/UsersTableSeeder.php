<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\Admin\Empleados\Asistencia;
use App\Models\Admin\Empleados\Planillas\Planilla;
use App\Models\Admin\Empleados\Planillas\Detalle;
use App\Models\User;

class UsersTableSeeder extends Seeder
{

    public function run()
    {
        $faker = \Faker\Factory::create();

            $user = new User;
            $user->name         = 'Admin';
            $user->username     = 'admin';
            $user->password     = Hash::make('admin');
            $user->tipo         = 'Administrador';
            $user->sucursal_id  = 1;
            $user->caja_id      = 1;
            $user->save();
            
            $user = new User;
            $user->name = 'Médico';
            $user->username = 'medico';
            $user->password = Hash::make('emple');
            $user->tipo = 'Médico';
            $user->codigo = '1234';
            $user->sucursal_id = 1;
            $user->save();

            $user = new User;
            $user->name = 'Cajero';
            $user->username = 'cajero';
            $user->password = Hash::make('emple');
            $user->tipo = 'Cajero';
            $user->caja_id = 1;
            $user->sucursal_id = 1;
            $user->save();

            $user = new User;
            $user->name = 'Empleado 1';
            $user->username = 'vendedor1';
            $user->password = Hash::make('emple');
            $user->tipo = 'Empleado';
            $user->sucursal_id = 1;
            $user->save();
            

            $user = new User;
            $user->name = 'Empleado 2';
            $user->username = 'vendedor2';
            $user->password = Hash::make('emple');
            $user->tipo = 'Empleado';
            $user->sucursal_id = 1;
            $user->save();
            

            $user = new User;
            $user->name = 'Empleado 3';
            $user->username = 'vendedor3';
            $user->password = Hash::make('emple');
            $user->tipo = 'Empleado';
            $user->sucursal_id = 1;
            $user->save();
            
            
            // $user = new User;
            // $user->name = 'Evelyn';
            // $user->username = 'evelyn';
            // $user->password = Hash::make('emple');
            // $user->tipo = 'Mesero';
            // $user->empleado = 1;
            // $user->sucursal_id = 1;
            // $user->save();

            //     for ($i=1; $i <= 17; $i++) { 
            //         $asistencia = new Asistencia;
            //         $asistencia->created_at = '2021-11-'. $i . ' 08:00:00';
            //         $asistencia->entrada = '2021-11-'. $i . ' 08:00:00';
            //         $asistencia->salida  = '2021-11-'. $i . ' 16:00:00';
            //         $asistencia->usuario_id  = $user->id;
            //         $asistencia->save();
            //     }
            
            // $user = new User;
            // $user->name = 'Gerardo';
            // $user->username = 'gerardo';
            // $user->password = Hash::make('emple');
            // $user->tipo = 'Mesero';
            // $user->empleado = 1;
            // $user->sucursal_id = 1;
            // $user->save();
            //     for ($i=1; $i <= 17; $i++) { 
            //         $asistencia = new Asistencia;
            //         $asistencia->created_at = '2021-11-'. $i . ' 08:00:00';
            //         $asistencia->entrada = '2021-11-'. $i . ' 08:00:00';
            //         $asistencia->salida  = '2021-11-'. $i . ' 16:00:00';
            //         $asistencia->usuario_id  = $user->id;
            //         $asistencia->save();
            //     }
            
            // $user = new User;
            // $user->name = 'Jonathan';
            // $user->username = 'jonathan';
            // $user->password = Hash::make('emple');
            // $user->tipo = 'Mesero';
            // $user->empleado = 1;
            // $user->sucursal_id = 1;
            // $user->save();
            //     for ($i=1; $i <= 17; $i++) { 
            //         $asistencia = new Asistencia;
            //         $asistencia->created_at = '2021-11-'. $i . ' 08:00:00';
            //         $asistencia->entrada = '2021-11-'. $i . ' 08:00:00';
            //         $asistencia->salida  = '2021-11-'. $i . ' 16:00:00';
            //         $asistencia->usuario_id  = $user->id;
            //         $asistencia->save();
            //     }



            // for ($i=1; $i <= 17; $i++) { 
            //     $table = new Planilla;
            //     $table->fecha       = $faker->dateTimeBetween($startDate = '-10 days', $endDate = 'now', $timezone = null);
            //     $table->total       = $faker->numberBetween(3250,1350);
            //     $table->usuario_id  = 1;
            //     $table->empresa_id  = 1;
            //     $table->save();

            //     for ($j=5; $j <= 7; $j++) { 
            //         $table2 = new Detalle;
            //         $table2->empleado_id = $j;
            //         $table2->dias        = 20;
            //         $table2->horas       = 1600;
            //         $table2->sueldo      = $faker->numberBetween(100,350);
            //         $table2->total       = $faker->numberBetween(100,350);
            //         $table2->planilla_id = $i;
            //         $table2->save();
            //     }

            // }


            
    }
}
