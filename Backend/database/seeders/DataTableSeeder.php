<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Pacientes\Consultas\Diagnosticos\Diagnostico;
use App\Models\Pacientes\Consultas\Examenes\Examen;
use File;

class DataTableSeeder extends Seeder {
     
    public function run()
    {
        $json = File::get(public_path()."/data/cie10.json");
        $array = json_decode($json, true);
        $array = collect($array);

        $json2 = File::get(public_path()."/data/examenes.json");
        $array2 = json_decode($json2, true);
        $array2 = collect($array2);

        for($i = 0; $i <= count($array) - 1 ; $i++)
        {
            $table = new Diagnostico;

            $table->codigo        = $array[$i]['code'];
            $table->descripcion = $array[$i]['description'];
            $table->empresa_id  = count($array2);
            $table->save();
            
        }


        for($i = 0; $i <= count($array2) - 1 ; $i++)
        {
            $table2 = new Examen;

            $table2->especialidad      = $array2[$i]['especialidad'];
            $table2->nombre            = $array2[$i]['examen'];
            if (isset($array2[$i]['etiquetas'])) {
                $table2->etiquetas   = $array2[$i]['etiquetas'];
            }

            $table2->empresa_id       = 1;
            $table2->save();
            
        }
    }
     
}
