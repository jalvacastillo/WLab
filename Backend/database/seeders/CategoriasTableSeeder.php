<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Inventario\Categorias\Categoria;
use App\Models\Inventario\Categorias\SubCategoria;

     
class CategoriasTableSeeder extends Seeder {
     
    public function run()
    {

        $faker = \Faker\Factory::create();


        Categoria::create(['nombre' => 'Servicios',            'empresa_id' => 1]);
                SubCategoria::create(['nombre' => 'Consultas',          'categoria_id' => 1]);
        
        Categoria::create(['nombre' => 'Medicamentos', 'empresa_id' => 1]);
                SubCategoria::create(['nombre' => 'Uno',        'categoria_id' => 2]);
                SubCategoria::create(['nombre' => 'Dos',           'categoria_id' => 2]);



    }
     
}
