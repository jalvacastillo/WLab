<?php 

use App\Http\Controllers\Api\Contabilidad\GastosController;

    Route::get('/gastos',             [GastosController::class, 'index']);
    Route::post('/gasto',             [GastosController::class, 'store']);
    Route::get('/gasto/{id}',         [GastosController::class, 'read']);
    Route::post('/gastos/filtrar',         [GastosController::class, 'filter']);
    Route::delete('/gasto/{id}',         [GastosController::class, 'delete']);


    Route::post('/gastos/dash',         [GastosController::class, 'dash']);

?>
