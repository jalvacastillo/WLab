<?php 

use App\Http\Controllers\Api\Empleados\PlanillasController;
use App\Http\Controllers\Api\Empleados\PlanillaDetallesController;


// Planilla
    Route::get('/planillas',                    [PlanillasController::class, 'index']);
    Route::get('/planillas/buscar/{text}',      [PlanillasController::class, 'search']);
    Route::post('/planillas/filtrar',           [PlanillasController::class, 'filter']);
    Route::post('/planilla',                    [PlanillasController::class, 'store']);
    Route::get('/planilla/{id}',                [PlanillasController::class, 'read']);
    Route::delete('/planilla/{id}',             [PlanillasController::class, 'delete']);
    Route::get('/planilla/reporte/{id}',       [PlanillasController::class, 'planilla']);
    Route::get('/planilla/boletas/{id}',       [PlanillasController::class, 'boletas']);

    Route::post('/planilla/detalle',            [PlanillaDetallesController::class, 'store']);
    Route::get('/planilla/detalle/{id}',        [PlanillaDetallesController::class, 'read']);
    Route::delete('/planilla/detalle/{id}',     [PlanillaDetallesController::class, 'delete']);


    
?>
