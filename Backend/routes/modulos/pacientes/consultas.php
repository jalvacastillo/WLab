<?php 

use App\Http\Controllers\Api\Pacientes\ConsultasController;
use App\Http\Controllers\Api\Pacientes\SignosController;
use App\Http\Controllers\Api\Pacientes\DiagnosticosController;
use App\Http\Controllers\Api\Pacientes\RecetasController;
use App\Http\Controllers\Api\Pacientes\ExamenesController;

// Consultas 
    Route::get('/consultas',                      [ConsultasController::class, 'index']);
    Route::get('/consultas/buscar/{txt}',           [ConsultasController::class, 'search']);
    Route::get('/consultas/list/{txt}',              [ConsultasController::class, 'list']);
    Route::post('/consultas/filtrar',                [ConsultasController::class, 'filter']);
    Route::get('/consulta/{id}',                    [ConsultasController::class, 'read']);
    Route::post('/consulta',                        [ConsultasController::class, 'store']);
    Route::delete('/consulta/{id}',                 [ConsultasController::class, 'delete']);

// Signos 
    Route::get('/consulta/signos',                      [SignosController::class, 'index']);
    Route::get('/consulta/signos/buscar/{txt}',           [SignosController::class, 'search']);
    Route::get('/consulta/signos/list/{txt}',              [SignosController::class, 'list']);
    Route::post('/consulta/signos/filtrar',                [SignosController::class, 'filter']);
    Route::get('/consulta/signo/{id}',                    [SignosController::class, 'read']);
    Route::post('/consulta/signo',                        [SignosController::class, 'store']);
    Route::delete('/consulta/signo/{id}',                 [SignosController::class, 'delete']);

// Diagnosticos 
    Route::get('/consulta/diagnosticos',             [DiagnosticosController::class, 'index']);
    Route::get('/consulta/diagnosticos/buscar/{txt}',  [DiagnosticosController::class, 'search']);
    Route::get('/consulta/diagnosticos/list/{txt}',     [DiagnosticosController::class, 'list']);
    Route::post('/consulta/diagnosticos/filtrar',       [DiagnosticosController::class, 'filter']);
    Route::get('/consulta/diagnostico/{id}',           [DiagnosticosController::class, 'read']);
    Route::post('/consulta/diagnostico',               [DiagnosticosController::class, 'store']);
    Route::delete('/consulta/diagnostico/{id}',        [DiagnosticosController::class, 'delete']);

// Diagnosticos 
    Route::get('/consulta/recetas',             [RecetasController::class, 'index']);
    Route::get('/consulta/recetas/buscar/{txt}',  [RecetasController::class, 'search']);
    Route::get('/consulta/recetas/list/{txt}',     [RecetasController::class, 'list']);
    Route::post('/consulta/recetas/filtrar',       [RecetasController::class, 'filter']);
    Route::get('/consulta/receta/{id}',           [RecetasController::class, 'read']);
    Route::post('/consulta/receta',               [RecetasController::class, 'store']);
    Route::delete('/consulta/receta/{id}',        [RecetasController::class, 'delete']);

    Route::get('/imprimir/receta/{id}',        [ConsultasController::class, 'imprimirReceta']);

// Examanes
    Route::get('/consulta/examenes',                      [ExamenesController::class, 'index']);
    Route::get('/consulta/examenes/buscar/{txt}',           [ExamenesController::class, 'search']);
    Route::get('/consulta/examenes/list/{txt}',              [ExamenesController::class, 'list']);
    Route::post('/consulta/examenes/filtrar',                [ExamenesController::class, 'filter']);
    Route::get('/consulta/examen/{id}',                    [ExamenesController::class, 'read']);
    Route::post('/consulta/examen',                        [ExamenesController::class, 'store']);
    Route::delete('/consulta/examen/{id}',                 [ExamenesController::class, 'delete']);
    
?>
