<?php 

use App\Http\Controllers\Api\Pacientes\PacientesController;

    Route::get('/pacientes',         				[PacientesController::class, 'index']);
	Route::get('/pacientes/buscar/{txt}',  			[PacientesController::class, 'search']);
    Route::get('/pacientes/list/{txt}',              [PacientesController::class, 'list']);
    Route::post('/pacientes/filtrar',                [PacientesController::class, 'filter']);
	Route::get('/paciente/{id}',     				[PacientesController::class, 'read']);
	Route::post('/paciente',         				[PacientesController::class, 'store']);
	Route::delete('/paciente/{id}',  				[PacientesController::class, 'delete']);

    Route::get('/paciente/expediente/{id}',                 [PacientesController::class, 'expediente']);

// Otros
    Route::get('/paciente/ventas/{id}',              [PacientesController::class, 'ventas']);
    Route::post('/paciente/ventas/filtrar',          [PacientesController::class, 'ventasFilter']);
    Route::get('/paciente/vales-pendientes/{id}',  	[PacientesController::class, 'valesPendientes']);
    Route::get('/paciente/anticipos/{id}',  			[PacientesController::class, 'anticipos']);

    Route::get('/cuentas-cobrar',                   [PacientesController::class, 'cxc']);
    Route::get('/cuentas-cobrar/buscar/{text}',     [PacientesController::class, 'cxcBuscar']);

?>
