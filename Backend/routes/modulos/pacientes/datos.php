<?php 

use App\Http\Controllers\Api\Pacientes\Diagnosticos\DiagnosticosController;
use App\Http\Controllers\Api\Pacientes\Examenes\ExamenesController;

    Route::get('/diagnosticos',                 [DiagnosticosController::class, 'index']);
    Route::get('/diagnosticos/buscar/{text}',   [DiagnosticosController::class, 'search']);
    Route::get('/diagnosticos/filtrar/{filtro}/{text}', [DiagnosticosController::class, 'filter']);
    Route::post('/diagnostico',                 [DiagnosticosController::class, 'store']);
    Route::get('/diagnostico/{id}',             [DiagnosticosController::class, 'read']);
    Route::delete('/diagnostico/{id}',          [DiagnosticosController::class, 'delete']);


    Route::get('/examenes',                 [ExamenesController::class, 'index']);
    Route::get('/examenes/buscar/{text}',   [ExamenesController::class, 'search']);
    Route::get('/examenes/filtrar/{filtro}/{text}', [ExamenesController::class, 'filter']);
    Route::post('/examen',                 [ExamenesController::class, 'store']);
    Route::get('/examen/{id}',             [ExamenesController::class, 'read']);
    Route::delete('/examen/{id}',          [ExamenesController::class, 'delete']);


?>
