<?php 

use App\Http\Controllers\Api\Admin\EmpresasController;
use App\Http\Controllers\Api\Admin\SucursalesController;
use App\Http\Controllers\Api\Admin\ReportesController;

    Route::get('/empresas',        	        [EmpresasController::class, 'index']);
    Route::post('/empresa',                 [EmpresasController::class, 'store']);
    Route::get('/empresa/{id}',             [EmpresasController::class, 'read']);

    Route::get('/sucursales',               [SucursalesController::class, 'index']);
    Route::post('/sucursal',                [SucursalesController::class, 'store']);
    Route::get('/sucursal/{id}',            [SucursalesController::class, 'read']);
    Route::delete('/sucursal/{id}',         [SucursalesController::class, 'delete']);

    Route::post('/reporte/requisicion-compras',    [ReportesController::class, 'requisicionCompra']);
    Route::get('/reporte/corte/{id}',              [ReportesController::class, 'corte']);


?>