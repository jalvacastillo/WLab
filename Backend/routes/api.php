<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/prueba', function(){ return Response()->json(['message' => 'Success'], 200); });

require base_path('routes/modulos/auth.php');
		
// Route::group(['middleware' => ['jwt.auth', 'ips']], function () {


		require base_path('routes/modulos/dash.php');

		require base_path('routes/modulos/facturacion.php');
	
	// Ventas
		require base_path('routes/modulos/ventas/ventas.php');
		require base_path('routes/modulos/ventas/detalles.php');
		require base_path('routes/modulos/ventas/devoluciones.php');
	
		require base_path('routes/modulos/ventas/clientes.php');

	// Compras
		require base_path('routes/modulos/compras/compras.php');
		require base_path('routes/modulos/compras/detalles.php');
		require base_path('routes/modulos/compras/devoluciones.php');

		require base_path('routes/modulos/compras/proveedores.php');

	// Ordenes
	    require base_path('routes/modulos/ordenes/ordenes.php');
	    require base_path('routes/modulos/ordenes/detalles.php');
	    // require base_path('routes/modulos/eventos/eventos.php');
	    require base_path('routes/modulos/eventos/detalles.php');
		require base_path('routes/modulos/eventos/servicios.php');

	// Inventario
		require base_path('routes/modulos/inventario/productos.php');
		require base_path('routes/modulos/inventario/servicios.php');
		require base_path('routes/modulos/inventario/materias-primas.php');
		require base_path('routes/modulos/inventario/inventarios.php');
		require base_path('routes/modulos/inventario/categorias.php');
		require base_path('routes/modulos/inventario/traslados.php');
		require base_path('routes/modulos/inventario/ajustes.php');

	// Creditos
		require base_path('routes/modulos/creditos/creditos.php');
		require base_path('routes/modulos/creditos/pagos.php');
		require base_path('routes/modulos/creditos/fiadores.php');

	// Pacientes
		require base_path('routes/modulos/pacientes/pacientes.php');
		require base_path('routes/modulos/pacientes/consultas.php');
		require base_path('routes/modulos/pacientes/citas.php');
		require base_path('routes/modulos/pacientes/datos.php');
		
	// Empleados
		require base_path('routes/modulos/empleados/empleados.php');
		require base_path('routes/modulos/empleados/planillas.php');
		require base_path('routes/modulos/empleados/comisiones.php');
		require base_path('routes/modulos/empleados/asistencias.php');
		
	// Contabilidad
		require base_path('routes/modulos/contabilidad/gastos.php');
		require base_path('routes/modulos/contabilidad/activos.php');
		require base_path('routes/modulos/contabilidad/cajas-chicas.php');

	// Admin
		require base_path('routes/modulos/admin/empresas.php');
		require base_path('routes/modulos/admin/bodegas.php');
		require base_path('routes/modulos/admin/cajas.php');
		require base_path('routes/modulos/admin/usuarios.php');
		require base_path('routes/modulos/admin/departamentos.php');
		require base_path('routes/modulos/admin/mensajes.php');

		
// });


Route::get('/prueba/factura', function () { 
	return view('reportes/pruebas/factura');	
});
