<?php

namespace App\Http\Controllers\Api\Admin\Cocina;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Admin\Cocina\Detalle;

class DetallesController extends Controller
{
    

    public function store(Request $request)
    {

        $request->validate([
            'categoria_id'       => 'required|numeric',
            'departamento_id'    => 'required|numeric',
        ]);

        $detalle = Detalle::where('departamento_id', $request->departamento_id)
                            ->where('categoria_id', $request->categoria_id)->first();

        if (!$detalle){
            if($request->id)
                $detalle = Detalle::findOrFail($request->id);
            else
                $detalle = new Detalle;
            
            $detalle->fill($request->all());
            $detalle->save();
        }

        return Response()->json($detalle, 200);

    }


    public function delete($id)
    {
       
        $detalle = Detalle::findOrFail($id);
        $detalle->delete();

        return Response()->json($detalle, 201);

    }


}
