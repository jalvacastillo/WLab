<?php

namespace App\Http\Controllers\Api\Ventas\Clientes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ventas\Clientes\Documento;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class DocumentosController extends Controller
{
    

    public function index($id) {
        
        $documento = Documento::where('cliente_id', $id)->get();
        return Response()->json($documento, 200);
    }

    public function read($id) {
        
        $documento = Documento::where('id', $id)->firstOrFail();
        return Response()->json($documento, 200);
    }

    public function filter(Request $request) {

        $documento = Documento::when($request->sucursal_id, function($query) use ($request){
                            return $query->where('sucursal_id', $request->sucursal_id);
                        })
                        ->when($request->tipo, function($query) use ($request){
                            return $query->where('tipo', $request->tipo);
                        })
                        ->orderBy('id','desc')->paginate(100000);

        return Response()->json($documento, 200);

    }

    public function search($txt) {

        $documento = Documento::where('nombre', 'like' ,'%' . $txt . '%')->paginate(15);
        return Response()->json($documento, 200);

    }


    public function store(Request $request)
    {
        $request->validate([
            'nombre'        => 'required|max:255',
            'file'          => 'required_without:url|max:5048',
            'nota'          => 'sometimes|max:255',
            'cliente_id'    => 'required',
        ],[
            'file.required_without' => 'Seleccione un documento a subir'
        ]
        );

        if($request->id)
            $documento = Documento::findOrFail($request->id);
        else
            $documento = new Documento;
        
        $documento->fill($request->all());

        if ($request->hasFile('file')) {
            if ($request->id && $documento->url) {
                Storage::delete($documento->url);
            }
            $file   = $request->file('file');
            $hash = md5($file->__toString());
            $path = "clientes/documentos/{$hash}." . $file->extension();
            $file->move(public_path('clientes/documentos/'),$hash . '.' . $file->extension());
            $documento->url = "/" . $path;
        }
        
        $documento->save();

        return Response()->json($documento, 200);


    }

    public function delete($id)
    {
       
        $documento = Documento::findOrFail($id);
        if ($documento->url) {
            Storage::delete($documento->url);
        }
        $documento->delete();

        return Response()->json($documento, 201);

    }


}
