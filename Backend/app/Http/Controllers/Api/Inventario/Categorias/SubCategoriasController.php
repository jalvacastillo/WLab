<?php

namespace App\Http\Controllers\Api\Inventario\Categorias;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Inventario\Categorias\SubCategoria;

class SubCategoriasController extends Controller
{
    

    public function index() {
       
        $subcategorias = SubCategoria::orderby('nombre', 'asc')->get();

        return Response()->json($subcategorias, 200);

    }


    public function read($id) {

        $subcategoria = SubCategoria::findOrFail($request->id);
        return Response()->json($subcategoria, 200);

    }


    public function store(Request $request)
    {
        $request->validate([
            'nombre'        => 'required|max:255',
            'categoria_id'  => 'required|numeric',
        ]);

        if($request->id)
            $subcategoria = SubCategoria::findOrFail($request->id);
        else
            $subcategoria = new SubCategoria;

        $subcategoria->fill($request->all());
        $subcategoria->save();

        return Response()->json($subcategoria, 200);

    }

    public function delete($id)
    {
        $subcategoria = SubCategoria::findOrFail($id);
        $subcategoria->delete();

        return Response()->json($subcategoria, 201);

    }


    public function change(Request $request){

        $request->validate([
            'subcategoria_anterior'  => 'required|numeric',
            'subcategoria_nueva'  => 'required|numeric',
        ]);

        $subcategoria = SubCategoria::findOrFail($request->subcategoria_anterior);

        foreach ($subcategoria->productos as $producto) {
            $producto->categoria_id = $request->subcategoria_nueva;
            $producto->save();
        }

        return Response()->json($subcategoria, 200);


    }


}
