<?php

namespace App\Http\Controllers\Api\Inventario;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Admin\Empresa;
use App\Models\Inventario\Producto;
use App\Models\Inventario\Ajuste;
use App\Models\Compras\Compra;
use App\Models\Compras\Detalle as DetalleCompra;
use App\Models\Ventas\Venta;
use App\Models\Ventas\Detalle as DetalleVenta;

class ProductosController extends Controller
{
    

    public function index() {
       
        $productos = Producto::where('tipo', 'Producto')->with('inventarios', 'sucursales')
                                // ->whereNull('codigo')
                                ->orderBy('id','desc')->paginate(10);

        return Response()->json($productos, 200);

    }

    public function list() {
       
        $productos = Producto::where('tipo', 'Producto')->orderby('nombre')->get();

        return Response()->json($productos, 200);

    }


    public function porCodigo($codigo) {
       
        // $producto = Producto::where('tipo', 'Producto')->where('codigo', $codigo )->with('inventarios')->first();
        $producto = Producto::where('tipo', 'Producto')->where('codigo', $codigo )->with('inventarios')->get();

        return Response()->json($producto, 200);

    }

    public function read($id) {

        $producto = Producto::where('tipo', 'Producto')->where('id', $id)->with('inventarios', 'composiciones', 'promociones', 'imagenes')->first();
        return Response()->json($producto, 200);

    }

    public function search($txt) {

        $productos = Producto::whereIn('tipo', ['Producto', 'Servicio'])->with('inventarios')
                                ->where('nombre', 'like' ,'%' . $txt . '%')
                                ->orwhere('codigo', 'like' ,'%' . $txt . '%')
                                ->paginate(10);
        return Response()->json($productos, 200);

    }

    public function searchAll($txt) {

        $productos = Producto::with('inventarios')
                                ->where('nombre', 'like' ,'%' . $txt . '%')
                                ->orwhere('codigo', 'like' ,'%' . $txt . '%')
                                ->paginate(10);
        return Response()->json($productos, 200);

    }

    public function filter(Request $request) {

            $productos = Producto::where('tipo', 'Producto')->with('inventarios', 'sucursales')
                                ->when($request->categoria_id, function($query) use ($request){
                                    return $query->where('categoria_id', $request->categoria_id);
                                })
                                ->when($request->stock_bodega, function($query) use ($request){
                                    return $query->where('inventario', true)->whereHas('inventarios', function($query){
                                        return $query->where('bodega_id', 1)->whereRaw('stock <= stock_min');
                                    });
                                })
                                ->when($request->stock_venta, function($query) use ($request){
                                    return $query->where('inventario', true)->whereHas('inventarios', function($query){
                                        return $query->where('bodega_id', 2)->whereRaw('stock <= stock_min');
                                    });
                                })
                                ->when($request->sin_control_inventario, function($query) use ($request){
                                    // return $query->whereDoesntHave('inventarios')->orwhere('inventario', false);
                                    return $query->where('inventario', false);
                                })
                                ->when($request->sin_condigo, function($query) use ($request){
                                    return $query->whereNull('codigo');
                                })
                                ->orderBy('id','desc')->paginate(100000);

            return Response()->json($productos, 200);
    }

    public function store(Request $request)
    {
        if(empty($request->codigo)){
            $request['codigo'] = NULL;
        }

        $request->validate([
            'nombre'    => 'required|max:255',
            'codigo'    => 'nullable|unique:productos,codigo,'. $request->id,
            'precio'    => 'required|numeric',
            'costo'     => 'required|numeric',
            'categoria_id'    => 'required',
            'empresa_id'    => 'required',
        ]);

        if($request->id)
            $producto = Producto::where('tipo', 'Producto')->findOrFail($request->id);
        else
            $producto = new Producto;
        
        $producto->fill($request->all());
        $producto->save();

        return Response()->json($producto, 200);

    }

    public function delete($id)
    {
        $producto = Producto::where('tipo', 'Producto')->findOrFail($id);
        foreach ($producto->inventarios as $bodega) {
            $bodega->delete();
        }
        $producto->delete();

        return Response()->json($producto, 201);

    }

    public function precios($id)
    {
        $producto = Producto::where('tipo', 'Producto')->findOrFail($id);
        
        
        $ventas = DetalleVenta::where('producto_id', $producto->id)->get();

        $ventas_precios =  collect();
        $ventas_fechas =  collect();

        foreach ($ventas->unique('precio') as $venta) {
            $ventas_precios->push($venta->precio);
            $ventas_fechas->push($venta->created_at->format('d/m/Y'));
        }
        $producto->ventas_precios = $ventas_precios;
        $producto->ventas_fechas = $ventas_fechas;
        $producto->ventas = count($ventas);

        return Response()->json($producto, 201);

    }


    public function analisis(Request $request) {


            $productos = Producto::where('tipo', 'Producto')->when($request->nombre, function($query) use ($request){
                                        return $query->where('nombre', 'like' ,'%' . $request->nombre . '%');
                                    })
                                    ->when($request->categoria_id, function($query) use ($request){
                                        return $query->where('categoria_id', $request->categoria_id);
                                    })

                                    ->get();

            $movimientos = collect();

            $empresa = Empresa::find(1);

            foreach ($productos as $producto) {
                if ($empresa->valor_inventario == 'Promedio') {
                    $producto->costo = $producto->costo_promedio;
                }
                $utilidad = $producto->precio - $producto->costo;
                $margen = $producto->costo > 0 ? (round($utilidad / $producto->costo, 2) * 100) : null;
                $movimientos->push([
                    'nombre'        => $producto->nombre,
                    'nombre_categoria'        => $producto->nombre_categoria,
                    'nombre_subcategoria'        => $producto->nombre_subcategoria,
                    // 'proveedor'     => $producto->proveedor,
                    'precio'        => $producto->precio,
                    'costo'         => $producto->costo,
                    'utilidad'      => $utilidad,
                    'margen'        =>  $margen
                ]);
            }

            return Response()->json($movimientos, 200);
    }

    public function compras(Request $request, $id) {

        $compras = Compra::whereHas('detalles', function($q) use ($id) {
                                    $q->where('producto_id', $id);
                                })
                                ->orderBy('id','desc')->paginate(5);
        

        return Response()->json($compras, 200);

    }

    public function ajustes(Request $request, $id) {

        $ajustes = Ajuste::where('producto_id', $id)->orderBy('id','desc')->paginate(5);
        
        return Response()->json($ajustes, 200);

    }

    public function ventas(Request $request, $id) {

        $ventas = Venta::whereHas('detalles', function($q) use ($id) {
                                    $q->where('producto_id', $id);
                                })
                                ->orderBy('id','desc')->paginate(5);
        
        return Response()->json($ventas, 200);

    }

    public function barcode($id) {

        $producto = Producto::findOrFail($id);
        
        return view('reportes.barcode', compact('producto'));

        
        $reportes = \PDF::loadView('reportes.barcode', compact('producto'))->setPaper('letter');
        return $reportes->stream();

    }

}
