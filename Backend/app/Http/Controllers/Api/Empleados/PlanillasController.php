<?php

namespace App\Http\Controllers\Api\Empleados;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Empleados\Planillas\Planilla;
use App\Models\Admin\Empresa;
use Carbon\Carbon;
use JWTAuth;

class PlanillasController extends Controller
{
    

    public function index() {
       
        $planillas = Planilla::orderBy('created_at', 'desc')->paginate(12);
        return Response()->json($planillas, 200);

    }


    public function read($id) {
        
        $planilla = Planilla::where('id', $id)->with('detalles')->firstOrFail();
        return Response()->json($planilla, 200);

    }

    public function filter(Request $request) {


        $planillas = Planilla::when($request->inicio, function($query) use ($request){
                            return $query->whereBetween('created_at', [$request->inicio . ' 00:00:00', $request->fin . ' 23:59:59']);
                        })
                        ->when($request->usuario_id, function($query) use ($request){
                            return $query->where('usuario_id', $request->usuario_id);
                        })
                        ->orderBy('created_at','desc')->paginate(100000);

        return Response()->json($planillas, 200);

    }

    public function store(Request $request) {
        
        $request->validate([
            'fecha_inicio'  => 'required|date',
            'fecha_fin'     => 'required|date',
            'total'         => 'required|numeric',
            'empresa_id'    => 'required|numeric',
        ]);

        if($request->id)
            $planilla = Planilla::findOrFail($request->id);
        else
            $planilla = new Planilla;
        
        $planilla->fill($request->all());
        $planilla->save();

        return Response()->json($planilla, 200);


    }

    public function search($txt) {

        $planillas = Planilla::where('nombre', 'like' ,'%' . $txt . '%')->get();
        return Response()->json($planillas, 200);

    }


    public function delete($id)
    {
       
        $planilla = Planilla::findOrFail($id);
        foreach ($planilla->detalles() as $detalle) {
            $detalle->delete();
        }
        $planilla->delete();

        return Response()->json($planilla, 201);

    }

    public function planilla($id) {

        $planilla = Planilla::where('id', $id)->with('detalles', 'empresa')->firstOrFail();

        $reportes = \PDF::loadView('reportes.empleados.planilla', compact('planilla'))->setPaper('letter', 'landscape');
        return $reportes->stream();

    }

    public function boletas($id) {

        $planilla = Planilla::where('id', $id)->with('detalles', 'empresa')->firstOrFail();

        $reportes = \PDF::loadView('reportes.empleados.boletas', compact('planilla'))->setPaper('letter');
        return $reportes->stream();

    }



}
