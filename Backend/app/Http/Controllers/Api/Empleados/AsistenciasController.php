<?php

namespace App\Http\Controllers\Api\Empleados;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Empleados\Asistencia;
use App\Models\Empleados\Empleado;
use App\Models\Admin\Empresa;
use Carbon\Carbon;
use JWTAuth;

class AsistenciasController extends Controller
{
    

    public function index() {
       
        $asistencias = Asistencia::orderBy('created_at', 'desc')->paginate(12);
        return Response()->json($asistencias, 200);

    }


    public function read($id) {
        
        $asistencia = Asistencia::findOrFail($id);
        return Response()->json($asistencia, 200);

    }

    public function filter(Request $request) {


        $asistencias = Asistencia::
                        when($request->inicio, function($query) use ($request){
                            return $query->whereBetween('entrada', [$request->inicio . ' 00:00:00', $request->fin . ' 23:59:59']);
                        })
                        ->when($request->usuario_id, function($query) use ($request){
                            return $query->where('usuario_id', $request->usuario_id);
                        })
                        ->orderBy('entrada','desc')->paginate(100000);

        return Response()->json($asistencias, 200);

    }

    public function store(Request $request) {
        
        $request->validate([
            'usuario_id'    => 'required|numeric',
        ]);

        $empleado = Empleado::findOrFail($request->usuario_id);

        $asistenciaDiaria = $empleado->asistenciaDiaria()->first();

        if($asistenciaDiaria){
            $asistencia = Asistencia::findOrFail($asistenciaDiaria->id);

            if ($asistencia->entrada && !$asistencia->salida) {
                $asistencia->salida = Carbon::now();
            }else{
                return Response()->json(['error' => ['Ya se han resgitrado la asistencia del día'], 'code' => 422], 422);
            }

            $asistencia->usuario_id = $empleado->id;
        }
        else{
            $asistencia = new Asistencia;
            $asistencia->entrada = Carbon::now();
            $asistencia->usuario_id = $empleado->id;
        }
        
        $asistencia->save();

        return Response()->json($asistencia, 200);


    }

    public function search($txt) {

        $asistencias = Asistencia::where('nombre', 'like' ,'%' . $txt . '%')->get();
        return Response()->json($asistencias, 200);

    }


    public function delete($id)
    {
       
        $asistencia = Asistencia::findOrFail($id);
        $asistencia->delete();

        return Response()->json($asistencia, 201);

    }

    public function empleados(){
        $fech = '2021-11-17';
        $fin = '2021-11-17';

        $empleados = Empleado::all();

        foreach ($empleados as $empleado) {
            $asistencia = $empleado->asistenciaDiaria()->first();

            $empleado->entrada = $asistencia ? $asistencia->entrada : null;
            $empleado->salida = $asistencia ? $asistencia->salida : null;
            $empleado->horas = $asistencia ? $asistencia->horas_laborales - $asistencia->horas_extras : null;
            $empleado->horas_laborales = $asistencia ? $asistencia->horas_laborales : null;
            $empleado->horas_extras = $asistencia ? $asistencia->horas_extras : null;
        }

        return Response()->json($empleados, 201);

    }

    public function asistenciaDiaria() {
        
        $empleados = Empleado::all();

        foreach ($empleados as $empleado) {
            $asistencia = $empleado->asistenciaDiaria()->first();

            $empleado->entrada = $asistencia ? $asistencia->entrada : null;
            $empleado->salida = $asistencia ? $asistencia->salida : null;
            $empleado->horas = $asistencia ? $asistencia->horas_laborales - $asistencia->horas_extras : null;
            $empleado->horas_laborales = $asistencia ? $asistencia->horas_laborales : null;
            $empleado->horas_extras = $asistencia ? $asistencia->horas_extras : null;
        }

        $empleados->fecha_inicio = Carbon::now();
        $empleados->fecha_fin = Carbon::now();
        $empleados->empresa = Empresa::where('id', JWTAuth::parseToken()->authenticate()->sucursal()->first()->empresa_id)->first();


        if ($empleados) {
            $reportes = \PDF::loadView('reportes.empleados.asistencia-diaria', compact('empleados'))->setPaper('letter', 'landscape');
            return $reportes->stream();
        }else{
            return "No hay asistencias entontradas";
        }

    }

    public function asistenciaMensual() {
        
        $empleados = Empleado::all();

            // return $empleados[1]->asistenciaMensual()->get();
        foreach ($empleados as $empleado) {
            if ($empleado->asistenciaMensual()->count() > 0) {
                $asistencias = $empleado->asistenciaMensual()->get();
                $empleado->horas = $asistencias ? $asistencias->sum('horas_laborales') - $asistencias->sum('horas_extras') : null;
                $empleado->horas_laborales = $asistencias ? $asistencias->sum('horas_laborales') : null;
                $empleado->horas_extras = $asistencias ? $asistencias->sum('horas_extras') : null;
                $empleado->dias = $asistencias->count();
            }
        }

        $empleados->fecha = Carbon::today();
        $empleados->empresa = Empresa::where('id', JWTAuth::parseToken()->authenticate()->sucursal()->first()->empresa_id)->first();


        if ($empleados) {
            $reportes = \PDF::loadView('reportes.empleados.asistencia-mensual', compact('empleados'))->setPaper('letter', 'landscape');
            return $reportes->stream();
        }else{
            return "No hay asistencias entontradas";
        }

    }



}
