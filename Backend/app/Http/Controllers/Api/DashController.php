<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use stdClass;

use App\Models\Admin\Empresa;
use App\Models\Admin\Caja;
use App\Models\Ventas\Venta;
use App\Models\Ventas\DevolucionVenta;
use App\Models\Ventas\Entrada;
use App\Models\Ventas\Detalle as VentaDetalle;
use App\Models\Compras\Compra;
use App\Models\Compras\Salida;
use App\Models\Admin\Corte;

use App\Models\Inventario\Producto;
use App\Models\Ventas\Clientes\Cliente;

use App\Models\User;


use App\Models\Eventos\Evento;
use App\Models\Eventos\Detalle as EventoDetalle;
use App\Models\Ordenes\Orden;
use App\Models\Ordenes\Detalle as OrdenDetalle;
use App\Models\Admin\Mesa;

use App\Models\Admin\Cocina\Departamento;

class DashController extends Controller
{

    public function index(Request $request) {

        $datos = new stdClass();

        // return $request;

        // Cajas
            $cajas = Caja::get();
            
            foreach ($cajas as $caja) {
                $caja->ventas_suma = $caja->ventasDia()->sum('total');
                $caja->ventas_cantidad = $caja->ventasDia()->count();
            }

            $datos->cajas = $cajas;

        // Ordenes
            $datos->total_ordenes = Orden::whereBetween('fecha', [$request->inicio, $request->fin])
                                            ->when($request->sucursal_id, function($q) use($request){
                                                $q->where('sucursal_id', $request->sucursal_id);
                                            })
                                            ->count();
            $datos->total_ordenes_semana   = Orden::
                                        selectRaw('DAY(fecha) as dia')
                                        ->selectRaw('count(id) as total')
                                        ->groupBy('dia')
                                        ->where('fecha', '>=', Carbon::now()->subDays(8))
                                        ->orderBy('dia')
                                        ->get();
            $ultima = $datos->total_ordenes_semana->sortByDesc('dia')->skip(1)->take(1)->pluck('total')->first();
            if ($ultima)
                $datos->total_ordenes_percent = round((($datos->total_ordenes / $ultima) - 1) * 100, 2);
            else
                $datos->total_ordenes_percent = 0;

        // Ventas
            $datos->total_ventas = Venta::whereBetween('fecha', [$request->inicio, $request->fin])
                                            ->when($request->sucursal_id, function($q) use($request){
                                                $q->where('sucursal_id', $request->sucursal_id);
                                            })
                                            ->sum('total');
            $datos->total_ventas_semana   = Venta::
                                        selectRaw('DAY(fecha) as dia')
                                        ->selectRaw('sum(total) as total')
                                        ->groupBy('dia')
                                        ->where('fecha', '>=', Carbon::now()->subDays(8))
                                        ->orderBy('dia')
                                        ->get();
            $ultima = $datos->total_ventas_semana->sortByDesc('dia')->skip(1)->take(1)->pluck('total')->first();
            if ($ultima)
                $datos->total_ventas_percent = round((($datos->total_ventas / $ultima) - 1) * 100, 2);
            else
                $datos->total_ventas_percent = 0;

        // Compras
            $datos->total_compras = Compra::whereBetween('fecha', [$request->inicio, $request->fin])
                                            ->sum('total');
            $datos->total_compras_semana   = Compra::
                                        selectRaw('DAY(fecha) as dia')
                                        ->selectRaw('sum(total) as total')
                                        ->groupBy('dia')
                                        ->where('fecha', '>=', Carbon::now()->subDays(8))
                                        ->orderBy('dia')
                                        ->get();
            $ultima = $datos->total_compras_semana->sortByDesc('dia')->skip(1)->take(1)->pluck('total')->first();
            if ($ultima)
                $datos->total_compras_percent = round((($datos->total_compras / $ultima) - 1) * 100, 2);
            else
                $datos->total_compras_percent = 0;

        // Utilidad
            $datos->total_utilidad = Venta::whereBetween('fecha', [$request->inicio, $request->fin])
                                            ->when($request->sucursal_id, function($q) use($request){
                                                $q->where('sucursal_id', $request->sucursal_id);
                                            })
                                            ->sum('subtotal');
            $datos->total_utilidad_semana   = Compra::
                                        selectRaw('DAY(fecha) as dia')
                                        ->selectRaw('sum(subtotal) as total')
                                        ->groupBy('dia')
                                        ->where('fecha', '>=', Carbon::now()->subDays(8))
                                        ->orderBy('dia')
                                        ->get();
            $ultima = $datos->total_utilidad_semana->sortByDesc('dia')->skip(1)->take(1)->pluck('subtotal')->first();
            if ($ultima)
                $datos->total_utilidad_percent = round((($datos->total_utilidad / $ultima) - 1) * 100, 2);
            else
                $datos->total_utilidad_percent = 0;

        // Productos

            $datos->productos   = VentaDetalle::
                                        selectRaw('sum(cantidad) AS total, producto_id, (select nombre from productos where producto_id = id) as nombre')
                                        ->groupBy('producto_id')
                                        ->whereHas('venta', function($q) use($request){
                                            $q->whereBetween('fecha', [$request->inicio, $request->fin])
                                            ->when($request->sucursal_id, function($q) use($request){
                                                $q->where('sucursal_id', $request->sucursal_id);
                                            });
                                        })
                                        ->orderBy('total', 'desc')
                                        ->take(5)
                                        ->get();

        // Ordenes

            $meseros    = User::where('tipo', 'Mesero')->get();
            $ordenes   =  Orden::whereBetween('fecha', [$request->inicio, $request->fin])
                                    ->when($request->sucursal_id, function($q) use($request){
                                        $q->where('sucursal_id', $request->sucursal_id);
                                    })
                                    ->get();
            foreach ($meseros as $mesero) {
                $mesero->ordenes = $ordenes->where('usuario_id', $mesero->id)->count();
            }

            $datos->meseros = $meseros->sortByDesc('ordenes')->values()->all();

            $datos->ticket_mayor       = round($ordenes->max('total'), 2);
            $datos->ticket_menor       = round($ordenes->min('total'), 2);
            $datos->promedio_ticket     = round($ordenes->avg('total'), 2);
            $datos->promedio_personas   = round($ordenes->avg('personas'), 0);
            $datos->total_personas      = round($ordenes->sum('personas'), 0);
            $datos->ticket_personas   = $datos->total_personas ? round($datos->total_ventas / $datos->total_personas, 2) : 0;

        // Mesas
            $mesas = $ordenes->groupBy('mesa')->values()->all();
            $movimientos = collect();
            foreach ($mesas as $mesa) {
                $movimientos->push([
                    'numero' => $mesa[0]->mesa,
                    'ordenes' => $mesa->count(),
                    'total' => $mesa->sum('total'),
                ]);
            }

            $datos->mesas = $movimientos->sortByDesc('total')->take(5)->values()->all();

        return Response()->json($datos, 200);
    }

    public function cocinero() {

        $datos = new stdClass();

        $ordenes    = Orden::where('fecha', '>=', Carbon::today())
                            ->with('detalles', 'cliente')
                            ->orderBy('id', 'asc')
                            ->get();
        $datos->ordenes        = $ordenes->whereIn('estado', ['Solicitada', 'En Proceso'] )->values()->all();
        $datos->ordenes         = $ordenes->where('estado', '!=', 'Cancelada')->count();
        $datos->despachadas     = $ordenes->whereIn('estado', ['Entregada'] )->count();
        $datos->en_proceso      = $ordenes->where('estado', 'En Proceso')->count();
        $datos->pendientes      = $ordenes->where('estado', 'Solicitada')->count();
        $datos->tiempo          = round($ordenes->avg('tiempo'),2);
        $datos->departamento = 0;

        return Response()->json($datos, 200);
    }

    public function cocineroDepartamento($id) {

        $datos = new stdClass();

        $departamento = Departamento::where('id', $id)->with('detalles')->firstOrFail();
        $categorias = $departamento->detalles()->pluck('categoria_id');

        // return $categorias;
        $detalles       = OrdenDetalle::whereHas('comanda', function($q){
                                $q->where('fecha', '>=', Carbon::today());
                            })
                            ->whereHas('producto', function($q) use($categorias){
                                $q->whereIn('categoria_id', $categorias );
                            })
                            ->with('comanda')
                            ->orderBy('id', 'asc')
                            ->get();

        $datos->detalles        = $detalles->whereIn('estado', ['Solicitada', 'En Proceso'] )->values()->all();
        $datos->ordenes         = $detalles->where('estado', '!=', 'Cancelada')->count();
        $datos->despachadas     = $detalles->whereIn('estado', ['Entregada'] )->count();
        $datos->en_proceso      = $detalles->where('estado', 'En Proceso')->count();
        $datos->pendientes      = $detalles->where('estado', 'Solicitada')->count();
        $datos->tiempo          = round($detalles->avg('tiempo'),2);

        $datos->departamento = $id;

        return Response()->json($datos, 200);
    }

    public function mesero() {

        $datos = new stdClass();

        $datos->ordenes    = Orden::whereNotIn('estado', ['Finalizada', 'Cancelada'] )
                            ->with('detalles', 'cliente')
                            ->orderBy('id', 'asc')
                            ->get();
                            
        $datos->tordenes         = Orden::where('estado', '!=', 'Cancelada')->count();
        $datos->despachadas     = Orden::whereIn('estado', ['Entregada'] )->count();
        $datos->en_proceso      = Orden::where('estado', 'En Proceso')->count();
        $datos->pendientes      = Orden::where('estado', 'Solicitada')->count();
        $datos->tiempo          = round(Orden::all()->avg('tiempo'),2);

        return Response()->json($datos, 200);
    }

    public function vendedor() {

        $datos = new stdClass();

        $usuario = \JWTAuth::parseToken()->authenticate();

        $datos->ordenes    = Orden::whereNotIn('estado', ['Finalizada', 'Cancelada'] )
                            ->with('detalles', 'cliente')
                            ->where('usuario_id', $usuario->id)
                            ->orderBy('id', 'asc')
                            ->get();
                            
        $datos->tordenes         = Orden::where('estado', '!=', 'Cancelada')->count();
        $datos->despachadas     = Orden::whereIn('estado', ['Entregada'] )->count();
        $datos->en_proceso      = Orden::where('estado', 'En Proceso')->count();
        $datos->pendientes      = Orden::where('estado', 'Solicitada')->count();
        $datos->tiempo          = round(Orden::all()->avg('tiempo'),2);

        return Response()->json($datos, 200);
    }

    public function cajero($id){

        // $datos = new stdClass();
        $usuario = User::findOrFail($id);
        $caja    = Caja::where('id', $usuario->caja_id)->with('corte')->firstOrFail();
        
        // Sino hay corte se muestran los datos del dia.
        if ($caja->corte) {
            $ordenes     = Orden::where('estado', 'Entregada')
                                    // ->where('fecha', '>=', Carbon::parse($caja->corte->apertura)->format('Y-m-d'))
                                    ->paginate(10000);
        } else {
            $ordenes     = Orden::where('estado', 'Entregada')
                                    ->where('fecha', '>=', Carbon::today())
                                    ->paginate(10000);
        }
        
        // Se saca el total de venta
        // $datos->ventas              = $ventas->sum('total');
        // $datos->ventas_efectivo     = $ventas->where('forma_de_pago','Efectivo')->sum('total');
        // $datos->ventas_tarjeta      = $ventas->where('forma_de_pago','Tarjeta')->sum('total');
        // $datos->ventas_vale         = $ventas->where('forma_de_pago','Vale')->sum('total');
        // $datos->ventas_cheque       = $ventas->where('forma_de_pago','Cheque')->sum('total');
        // $datos->ventas_versatec     = $ventas->where('forma_de_pago','Versatec')->sum('total');

        return Response()->json($ordenes, 200);
    }



}
