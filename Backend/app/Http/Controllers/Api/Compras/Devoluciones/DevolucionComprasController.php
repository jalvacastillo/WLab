<?php

namespace App\Http\Controllers\Api\Compras\Devoluciones;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Compras\Devoluciones\Devolucion;
use App\Models\Registros\Proveedor;
use App\Models\Compras\Devoluciones\Detalle;
use App\Models\Inventario\Producto;
use App\Models\Inventario\Inventario;
use App\Models\Inventario\Kardex;
use App\Models\Admin\Tanque;
use Illuminate\Support\Facades\DB;

class DevolucionComprasController extends Controller
{
    

    public function index() {
       
        $compras = Devolucion::orderBy('id','desc')->paginate(10);
        return Response()->json($compras, 200);
           
    }

    public function read($id) {

        $compra = Devolucion::where('id', $id)->with('detalles', 'proveedor')->first();
        return Response()->json($compra, 200);
 
    }

    public function search($txt) {

        $compras = Devolucion::whereHas('proveedor', function($query) use ($txt)
                    {
                        $query->where('nombre', 'like' ,'%' . $txt . '%');
                    })->paginate(10);

        return Response()->json($compras, 200);

    }

    public function filter(Request $request) {

        $compras = Devolucion::when($request->inicio, function($query) use ($request){
                                return $query->whereBetween('fecha', [$request->inicio, $request->fin]);
                            })
                            ->when($request->estado, function($query) use ($request){
                                return $query->where('estado', $request->estado);
                            })
                            ->when($request->proveedor_id, function($query) use ($request){
                                return $query->whereHas('proveedor', function($query) use ($request)
                                {
                                    $query->where('proveedor_id', $request->proveedor_id);

                                });
                            })
                            ->orderBy('id','desc')->paginate(100000);

        return Response()->json($compras, 200);

    }



    public function store(Request $request)
    {

        $request->validate([
            'fecha'             => 'required',
            'estado'            => 'required',
            'proveedor_id'      => 'required',
            'usuario_id'        => 'required',
        ]);

        if($request->id)
            $compra = Devolucion::findOrFail($request->id);
        else
            $compra = new Devolucion;
        
        $compra->fill($request->all());
        $compra->save();

        return Response()->json($compra, 200);

    }

    public function delete($id)
    {
        $compra = Devolucion::where('id', $id)->with('detalles')->firstOrFail();
        foreach ($compra->detalles as $detalle) {
            $detalle->delete();
        }
        $compra->delete();

        return Response()->json($compra, 201);
    }


    public function facturacion(Request $request){

        $request->validate([
            'fecha'             => 'required',
            'tipo'              => 'required',
            'proveedor'         => 'required',
            'detalles'          => 'required',
            'iva'               => 'required|numeric',
            // 'subcosto'          => 'required|numeric',
            'subtotal'          => 'required|numeric',
            'total'             => 'required|numeric',
            'nota'              => 'max:255',
            'usuario_id'        => 'required',
            'empresa_id'        => 'required',
        ]);


        DB::beginTransaction();
         
        try {
        

        // Guardamos el proveedor

            if(isset($request->proveedor['id']))
                $proveedor = Proveedor::findOrFail($request->proveedor['id']);
            else
                $proveedor = new Proveedor;

            $proveedor->fill($request->proveedor);
            $proveedor->save();

        // Compra
            if($request->id)
                $compra = Devolucion::findOrFail($request->id);
            else
                $compra = new Devolucion;

            $request['proveedor_id'] = $proveedor->id;
            $compra->fill($request->all());
            $compra->save();


        // Detalles

            foreach ($request->detalles as $det) {
                if(isset($det['id']))
                    $detalle = Detalle::findOrFail($det['id']);
                else
                    $detalle = new Detalle;

                $det['compra_id'] = $compra->id;
                
                $detalle->fill($det);
                
                if (!isset($det['id'])) {
                    // Actualizar inventario
                    $producto = Producto::findOrFail($det['producto_id']);
                    if (isset($det['inventario_id']) && $producto->inventario){

                        $inventario = Inventario::where('producto_id', $producto->id)->where('id', $det['inventario_id'])->first();

                        if ($inventario) {
                            $inventario->stock -= $det['cantidad'];
                            $inventario->save();

                            // Kardex

                                $valor = $det['costo'];
                                $entradaCantidad =  null;
                                $salidaCantidad =  $det['cantidad'];
                                Kardex::create([
                                    'fecha'             => date('Y-m-d'),
                                    'producto_id'       => $producto->id,
                                    'bodega_id'         => $inventario->bodega_id,
                                    'detalle'           => 'Devolucion de compra',
                                    'referencia'        => $compra->id,
                                    'valor_unitario'    => $valor,
                                    'entrada_cantidad'  => $entradaCantidad,
                                    'entrada_valor'     => $entradaCantidad ? $entradaCantidad * $valor : null,
                                    'salida_cantidad'   => $salidaCantidad,
                                    'salida_valor'      => $salidaCantidad ? $salidaCantidad * $valor : null,
                                    'total_cantidad'    => $inventario->stock,
                                    'total_valor'       => $inventario->stock * $valor,
                                    'usuario_id'        => $request->usuario_id,
                                ]);
                        }

                    }
                    $producto->precio           = isset($det['precio_nuevo']) ? $det['precio_nuevo'] : $producto->precio ;
                    $producto->costo_anterior   = $producto->costo;
                    $producto->costo            = isset($det['costo']) ? $det['costo'] : $producto->costo ;
                    $producto->save();

                }

                $detalle->save();
            }

        DB::commit();
        return Response()->json($compra, 200);

        } catch (\Exception $e) {
            DB::rollback();
            return Response()->json(['error' => $e->getMessage()], 400);
        } catch (\Throwable $e) {
            DB::rollback();
            return Response()->json(['error' => $e->getMessage()], 400);
        }
        
        return Response()->json($compra, 200);

    }



}
