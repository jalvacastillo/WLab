<?php

namespace App\Http\Controllers\Api\Pacientes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use JWTAuth;
use Carbon\Carbon;

use App\Models\Pacientes\Consultas\Diagnostico;

class DiagnosticosController extends Controller
{
    

    public function index() {
       
        $diagnosticos = Diagnostico::orderBy('id','desc')->paginate(10);
       
        return Response()->json($diagnosticos, 200);

    }



    public function read($id) {

        $diagnostico = Diagnostico::where('id', $id)->first();
        return Response()->json($diagnostico, 200);

    }

    public function search($txt) {

        $diagnosticos = Diagnostico::where('codigo', 'like', '%'.$txt.'%')
                                ->orwhere('descripcion', 'like', '%'.$txt.'%')
                                ->paginate(10);

        return Response()->json($diagnosticos, 200);

    }

    public function filter(Request $request) {


        $diagnosticos = Diagnostico::when($request->inicio, function($query) use ($request){
                            return $query->whereBetween('fecha', [$request->inicio, $request->fin]);
                        })
                        ->when($request->usuario_id, function($query) use ($request){
                            return $query->where('usuario_id', $request->usuario_id);
                        })
                        ->when($request->estado, function($query) use ($request){
                            return $query->where('estado', $request->estado);
                        })
                        ->when($request->metodo_pago, function($query) use ($request){
                            return $query->where('metodo_pago', $request->metodo_pago);
                        })
                        ->when($request->tipo_documento, function($query) use ($request){
                            return $query->where('tipo_documento', $request->tipo_documento);
                        })
                        ->orderBy('id','desc')->paginate(100000);

        return Response()->json($diagnosticos, 200);

    }

    public function store(Request $request)
    {
        $request->validate([
            'codigo'             => 'required',
            'descripcion'        => 'required',
            'consulta_id'       => 'required',
        ]);

        if($request->id)
            $diagnostico = Diagnostico::findOrFail($request->id);
        else
            $diagnostico = new Diagnostico;
        
        $diagnostico->fill($request->all());
        $diagnostico->save();        

        return Response()->json($diagnostico, 200);

    }

    public function delete($id)
    {
        $diagnostico = Diagnostico::findOrFail($id);
        $diagnostico->delete();

        return Response()->json($diagnostico, 201);

    }




}
