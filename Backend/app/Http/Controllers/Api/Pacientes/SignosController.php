<?php

namespace App\Http\Controllers\Api\Pacientes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use JWTAuth;
use Carbon\Carbon;

use App\Models\Pacientes\Consultas\Signo;

class SignosController extends Controller
{
    

    public function index() {
       
        $signos = Signo::orderBy('id','desc')->paginate(10);
       
        return Response()->json($signos, 200);

    }



    public function read($id) {

        $signo = Signo::where('id', $id)->first();
        return Response()->json($signo, 200);

    }

    public function search($txt) {

        $signos = Signo::whereHas('cliente', function($query) use ($txt) {
                                    $query->where('nombre', 'like' ,'%' . $txt . '%');
                                })
                                ->orwhere('correlativo', 'like', '%'.$txt.'%')
                                ->orwhere('tipo_documento', 'like', '%'.$txt.'%')
                                ->orwhere('estado', 'like', '%'.$txt.'%')
                                ->orwhere('metodo_pago', 'like', '%'.$txt.'%')
                                ->orwhere('referencia', 'like', '%'.$txt.'%')
                                ->paginate(10);

        return Response()->json($signos, 200);

    }

    public function filter(Request $request) {


        $signos = Signo::when($request->inicio, function($query) use ($request){
                            return $query->whereBetween('fecha', [$request->inicio, $request->fin]);
                        })
                        ->when($request->usuario_id, function($query) use ($request){
                            return $query->where('usuario_id', $request->usuario_id);
                        })
                        ->when($request->estado, function($query) use ($request){
                            return $query->where('estado', $request->estado);
                        })
                        ->when($request->metodo_pago, function($query) use ($request){
                            return $query->where('metodo_pago', $request->metodo_pago);
                        })
                        ->when($request->tipo_documento, function($query) use ($request){
                            return $query->where('tipo_documento', $request->tipo_documento);
                        })
                        ->orderBy('id','desc')->paginate(100000);

        return Response()->json($signos, 200);

    }

    public function store(Request $request)
    {
        $request->validate([
            'fecha'             => 'required',
            'peso'              => 'required',
            'peso_unidad'       => 'required',
            'estatura'          => 'required',
            'estatura_unidad'   => 'required',
            'temperatura'       => 'required',
            'temperatura_unidad'=> 'required',
            'consulta_id'       => 'required',
            'usuario_id'        => 'required',
        ]);

        if($request->id)
            $signo = Signo::findOrFail($request->id);
        else
            $signo = new Signo;
        
        $signo->fill($request->all());
        $signo->save();        

        return Response()->json($signo, 200);

    }

    public function delete($id)
    {
        $signo = Signo::findOrFail($id);
        $signo->delete();

        return Response()->json($signo, 201);

    }




}
