<?php

namespace App\Http\Controllers\Api\Pacientes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use JWTAuth;
use Carbon\Carbon;

use App\Models\Pacientes\Consultas\Consulta;
use App\Models\Pacientes\Paciente;

class ConsultasController extends Controller
{
    

    public function index() {
       
        $consultas = Consulta::orderBy('id','desc')->paginate(10);
       
        return Response()->json($consultas, 200);

    }



    public function read($id) {

        $consulta = Consulta::where('id', $id)->with('signos','diagnosticos','recetas','examenes', 'paciente')->first();
        return Response()->json($consulta, 200);

    }

    public function search($txt) {

        $consultas = Consulta::whereHas('paciente', function($query) use ($txt) {
                                    $query->where('nombre', 'like' ,'%' . $txt . '%');
                                })
                                ->orwhere('correlativo', 'like', '%'.$txt.'%')
                                ->orwhere('tipo_documento', 'like', '%'.$txt.'%')
                                ->orwhere('estado', 'like', '%'.$txt.'%')
                                ->orwhere('metodo_pago', 'like', '%'.$txt.'%')
                                ->orwhere('referencia', 'like', '%'.$txt.'%')
                                ->paginate(10);

        return Response()->json($consultas, 200);

    }

    public function filter(Request $request) {


        $consultas = Consulta::when($request->inicio, function($query) use ($request){
                            return $query->whereBetween('fecha', [$request->inicio, $request->fin]);
                        })
                        ->when($request->usuario_id, function($query) use ($request){
                            return $query->where('usuario_id', $request->usuario_id);
                        })
                        ->when($request->estado, function($query) use ($request){
                            return $query->where('estado', $request->estado);
                        })
                        ->when($request->metodo_pago, function($query) use ($request){
                            return $query->where('metodo_pago', $request->metodo_pago);
                        })
                        ->when($request->tipo_documento, function($query) use ($request){
                            return $query->where('tipo_documento', $request->tipo_documento);
                        })
                        ->orderBy('id','desc')->paginate(100000);

        return Response()->json($consultas, 200);

    }

    public function store(Request $request)
    {
        $request->validate([
            'fecha'             => 'required',
            'estado'            => 'required',
            'tipo'              => 'required',
            'motivo'            => 'required|max:255',
            'paciente.nombre'   => 'required',
            'paciente.fecha_nacimiento'   => 'required',
            'paciente.id'       => 'required_if:paciente.nombre, ""',
            'usuario_id'        => 'required',
            'sucursal_id'       => 'required',
        ], [
            'paciente.nombre.required' => 'Seleccione o agregue los datos del paciente'
        ]);

        // Guardamos el paciente

            if(isset($request->paciente['id'])){
                $paciente = Paciente::findOrFail($request->paciente['id']);
            }
            else{
                $paciente = new Paciente;
                $paciente->fecha = Carbon::now();
                $paciente->empresa_id = JWTAuth::parseToken()->authenticate()->sucursal_id;
            }

            $paciente->fill($request->paciente);
            $paciente->save();

        if($request->id)
            $consulta = Consulta::findOrFail($request->id);
        else
            $consulta = new Consulta;

        $request['paciente_id'] = $paciente->id;
        
        $consulta->fill($request->all());
        $consulta->save();        

        return Response()->json($consulta, 200);

    }

    public function delete($id)
    {
        $consulta = Consulta::findOrFail($id);

        foreach ($consulta->detalles as $detalle) {
            // Actualizar inconsultario
                $producto = Producto::findOrFail($detalle->producto_id);

                if ($producto->inconsultario) {
                    $bodega = Inconsultario::where('producto_id', $producto->id)->where('bodega_id', 2)->increment('stock', $detalle->cantidad);
                }

            $detalle->delete();
        }
        $consulta->delete();

        return Response()->json($consulta, 201);

    }

    public function imprimirReceta($consulta_id){
       $consulta = Consulta::where('id', $consulta_id)->with('paciente', 'recetas', 'sucursal')->firstOrFail();

       $view = \PDF::loadView('reportes.receta', compact('consulta'));
       return $view->stream();

   }

   public function examenes($consulta_id){
       $consulta = Consulta::where('id', $consulta_id)->with('paciente', 'examenes', 'sucursal')->firstOrFail();

       $view = \PDF::loadView('reportes.examenes', compact('consulta'));
       return $view->stream();

   }





}
