<?php

namespace App\Http\Controllers\Api\Pacientes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use JWTAuth;
use Carbon\Carbon;

use App\Models\Pacientes\Cita;
use App\Models\Pacientes\Paciente;

class CitasController extends Controller
{
    

    public function index() {
       
        $citas = Cita::orderBy('id','desc')->paginate(10);
       
        return Response()->json($citas, 200);

    }



    public function read($id) {

        $cita = Cita::where('id', $id)->with('signos','diagnosticos','recetas','examenes', 'paciente')->first();
        return Response()->json($cita, 200);

    }

    public function search($txt) {

        $citas = Cita::whereHas('paciente', function($query) use ($txt) {
                                    $query->where('nombre', 'like' ,'%' . $txt . '%');
                                })
                                ->orwhere('correlativo', 'like', '%'.$txt.'%')
                                ->orwhere('tipo_documento', 'like', '%'.$txt.'%')
                                ->orwhere('estado', 'like', '%'.$txt.'%')
                                ->orwhere('metodo_pago', 'like', '%'.$txt.'%')
                                ->orwhere('referencia', 'like', '%'.$txt.'%')
                                ->paginate(10);

        return Response()->json($citas, 200);

    }

    public function filter(Request $request) {


        $citas = Cita::when($request->inicio, function($query) use ($request){
                            return $query->whereBetween('fecha', [$request->inicio, $request->fin]);
                        })
                        ->when($request->usuario_id, function($query) use ($request){
                            return $query->where('usuario_id', $request->usuario_id);
                        })
                        ->when($request->estado, function($query) use ($request){
                            return $query->where('estado', $request->estado);
                        })
                        ->when($request->metodo_pago, function($query) use ($request){
                            return $query->where('metodo_pago', $request->metodo_pago);
                        })
                        ->when($request->tipo_documento, function($query) use ($request){
                            return $query->where('tipo_documento', $request->tipo_documento);
                        })
                        ->orderBy('id','desc')->paginate(100000);

        return Response()->json($citas, 200);

    }

    public function store(Request $request)
    {
        $request->validate([
            'fecha'             => 'required',
            'estado'            => 'required',
            'tipo'              => 'required',
            'motivo'            => 'required|max:255',
            'paciente'          => 'required',
            'usuario_id'        => 'required',
            'sucursal_id'       => 'required',
        ]);

        // Guardamos el paciente

            if(isset($request->paciente['id'])){
                $paciente = Paciente::findOrFail($request->paciente['id']);
            }
            else{
                $paciente = new Paciente;
                $paciente->empresa_id = JWTAuth::parseToken()->authenticate()->sucursal_id;
            }

            $paciente->fill($request->paciente);
            $paciente->save();

        if($request->id)
            $cita = Cita::findOrFail($request->id);
        else
            $cita = new Cita;

        $request['paciente_id'] = $paciente->id;
        
        $cita->fill($request->all());
        $cita->save();        

        return Response()->json($cita, 200);

    }

    public function delete($id)
    {
        $cita = Cita::findOrFail($id);

        foreach ($cita->detalles as $detalle) {
            // Actualizar incitario
                $producto = Producto::findOrFail($detalle->producto_id);

                if ($producto->incitario) {
                    $bodega = Incitario::where('producto_id', $producto->id)->where('bodega_id', 2)->increment('stock', $detalle->cantidad);
                }

            $detalle->delete();
        }
        $cita->delete();

        return Response()->json($cita, 201);

    }

    public function imprimirReceta($cita_id){
       $cita = Cita::where('id', $cita_id)->with('paciente', 'recetas', 'sucursal')->firstOrFail();

       $view = \PDF::loadView('reportes.receta', compact('cita'));
       return $view->stream();

   }

   public function examenes($cita_id){
       $cita = Cita::where('id', $cita_id)->with('paciente', 'examenes', 'sucursal')->firstOrFail();

       $view = \PDF::loadView('reportes.examenes', compact('cita'));
       return $view->stream();

   }





}
