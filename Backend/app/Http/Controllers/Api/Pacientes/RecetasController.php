<?php

namespace App\Http\Controllers\Api\Pacientes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use JWTAuth;
use Carbon\Carbon;

use App\Models\Pacientes\Consultas\Receta;

class RecetasController extends Controller
{
    

    public function index() {
       
        $recetas = Receta::orderBy('id','desc')->paginate(10);
       
        return Response()->json($recetas, 200);

    }



    public function read($id) {

        $receta = Receta::where('id', $id)->first();
        return Response()->json($receta, 200);

    }

    public function search($txt) {

        $recetas = Receta::whereHas('cliente', function($query) use ($txt) {
                                    $query->where('nombre', 'like' ,'%' . $txt . '%');
                                })
                                ->orwhere('correlativo', 'like', '%'.$txt.'%')
                                ->orwhere('tipo_documento', 'like', '%'.$txt.'%')
                                ->orwhere('estado', 'like', '%'.$txt.'%')
                                ->orwhere('metodo_pago', 'like', '%'.$txt.'%')
                                ->orwhere('referencia', 'like', '%'.$txt.'%')
                                ->paginate(10);

        return Response()->json($recetas, 200);

    }

    public function filter(Request $request) {


        $recetas = Receta::when($request->inicio, function($query) use ($request){
                            return $query->whereBetween('fecha', [$request->inicio, $request->fin]);
                        })
                        ->when($request->usuario_id, function($query) use ($request){
                            return $query->where('usuario_id', $request->usuario_id);
                        })
                        ->when($request->estado, function($query) use ($request){
                            return $query->where('estado', $request->estado);
                        })
                        ->when($request->metodo_pago, function($query) use ($request){
                            return $query->where('metodo_pago', $request->metodo_pago);
                        })
                        ->when($request->tipo_documento, function($query) use ($request){
                            return $query->where('tipo_documento', $request->tipo_documento);
                        })
                        ->orderBy('id','desc')->paginate(100000);

        return Response()->json($recetas, 200);

    }

    public function store(Request $request)
    {
        $request->validate([
            'producto_id'       => 'required|numeric',
            'cantidad'          => 'required|numeric',
            // 'dosis'             => 'required|max:500',
            'consulta_id'       => 'required|numeric',
        ]);

        if($request->id)
            $receta = Receta::findOrFail($request->id);
        else
            $receta = new Receta;
        
        $receta->fill($request->all());
        $receta->save();        

        return Response()->json($receta, 200);

    }

    public function delete($id)
    {
        $receta = Receta::findOrFail($id);
        $receta->delete();

        return Response()->json($receta, 201);

    }




}
