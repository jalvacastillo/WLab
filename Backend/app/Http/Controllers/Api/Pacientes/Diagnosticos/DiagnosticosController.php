<?php

namespace App\Http\Controllers\Api\Pacientes\Diagnosticos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Pacientes\Consultas\Diagnosticos\Diagnostico;

class DiagnosticosController extends Controller
{
    

    public function index() {
       
        $diagnosticos = Diagnostico::orderBy('id','desc')->paginate(10);

        return Response()->json($diagnosticos, 200);

    }


    public function read($id) {
        
        $diagnostico = Diagnostico::where('id', $id)->firstOrFail();
        return Response()->json($diagnostico, 200);
    }

    public function filter($campo, $valor) {
        
        $diagnostico = Diagnostico::where($campo, $valor)->paginate(10);

        return Response()->json($diagnostico, 200);
    }

    public function search($txt) {

        $diagnosticos = Diagnostico::where('codigo', 'like' ,'%' . $txt . '%')
                        ->orwhere('descripcion', 'like' ,'%' . $txt . '%')
                        ->orwhere('equivalencias', 'like' ,'%' . $txt . '%')
                        ->paginate(10);


        return Response()->json($diagnosticos, 200);

    }


    public function store(Request $request)
    {
        $request->validate([
            'code'          => 'required|max:255',
            'descripcion'   => 'required|max:500',
            'equivalencias' => 'sometimes|max:500',
            'empresa_id'    => 'required|numeric',
        ]);

        $diagnostico->fill($request->all());
        $diagnostico->save();

        return Response()->json($diagnostico, 200);


    }

    public function delete($id)
    {
       
        $diagnostico = Diagnostico::findOrFail($id);
        $diagnostico->delete();

        return Response()->json($diagnostico, 201);

    }



}
