<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model {

    protected $table = 'empresa_sucursales';
    protected $fillable = [
        'nombre',
        'telefono',
        'correo',
        'direccion',
        'municipio',
        'departamento',
        'direccion',
        'empresa_id',
    ];

    public function cajas(){
        return $this->hasMany('App\Models\Admin\Caja', 'sucursal_id');
    }

    public function bodegas(){
        return $this->hasMany('App\Models\Admin\Bodega', 'sucursal_id');
    }

    public function departamentos(){
        return $this->hasMany('App\Models\Admin\Cocina\Departamento', 'sucursal_id');
    }

    public function usuarios(){
        return $this->hasMany('App\Models\User', 'sucursal_id');
    }

    public function empresa(){
        return $this->belongsTo('App\Models\Admin\Empresa', 'empresa_id');
    }


}
