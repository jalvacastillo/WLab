<?php

namespace App\Models\Admin\Cocina;

use Illuminate\Database\Eloquent\Model;

class Detalle extends Model {

    protected $table = 'cocina_departamento_detalles';
    protected $fillable = array(
        'categoria_id',
        'departamento_id'
    );

    protected $appends = ['nombre_departamento', 'nombre_categoria'];

    public function getNombreDepartamentoAttribute()
    {
        return $this->departamento()->pluck('nombre')->first();
    }

    public function getNombreCategoriaAttribute()
    {
        return $this->categoria()->pluck('nombre')->first();
    }

    public function categoria(){
        return $this->belongsTo('App\Models\Inventario\Categorias\SubCategoria', 'categoria_id');
    }

    public function departamento(){
        return $this->belongsTo('App\Models\Admin\Cocina\Departamento', 'departamento_id');
    }


}



