<?php

namespace App\Models\Admin\Cocina;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model {

    protected $table = 'cocina_departamentos';
    protected $fillable = array(
        'nombre',
        'sucursal_id'
    );

    protected $appends = ['nombre_sucursal', 'num_detalles'];

    public function getNumDetallesAttribute()
    {
        return $this->detalles->count();
    }

    public function getNombreSucursalAttribute()
    {
        return $this->sucursal()->pluck('nombre')->first();
    }

    public function detalles(){
        return $this->hasMany('App\Models\Admin\Cocina\Detalle');
    }

    public function sucursal(){
        return $this->belongsTo('App\Models\Admin\Sucursal', 'sucursal_id');
    }


}



