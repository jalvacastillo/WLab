<?php

namespace App\Models\Contabilidad;

use Illuminate\Database\Eloquent\Model;

class Activo extends Model {

    protected $table = 'empresa_activos';
    protected $fillable = array(
        'fecha_compra',
        'nombre',
        'descripcion',
        'ubicacion',
        'costo',
        'empresa_id',
    );

    protected $appends = ['nombre_usuario', 'nombre_sucursal'];

    public function getNombreUsuarioAttribute(){
        return $this->usuario()->pluck('name')->first();
    }
    public function getNombreSucursalAttribute(){
        return $this->sucursal()->pluck('nombre')->first();
    }

    public function usuario(){
        return $this->belongsTo('App\Models\User', 'usuario_id');
    }

    public function sucursal(){
        return $this->belongsTo('App\Models\Admin\Sucursal', 'sucursal_id');
    }


}



