<?php

namespace App\Models\Empleados;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Models\Admin\Empresa;

class Contrato extends Model {

    protected $table = 'empleados_contratos';
    protected $fillable = array(
        'fecha',
        'sueldo',
        'dias',
        'horas',
        'renta',
        'afp',
        'isss',
        'empleado_id'
    );

    protected $appends = ['nombre_empleado', 'horas_laborales', 'horas_extras'];
    protected $casts = ['afp' => 'boolean', 'isss' => 'boolean', 'renta' => 'boolean'];

    public function getHorasLaboralesAttribute()
    {
        if ($this->salida)
            return Carbon::parse($this->entrada)->diffInHours(Carbon::parse($this->salida));
        else
            return Carbon::parse($this->entrada)->diffInHours(Carbon::now());
    }

    public function getHorasExtrasAttribute()
    {
        $empresa = Empresa::find($this->empleado()->first()->sucursal()->first()->empresa_id);

        if ($this->horas_laborales > $empresa->horas_laborales)
            return $this->horas_laborales - $empresa->horas_laborales;
        else
            return 0;
    }

    public function getNombreEmpleadoAttribute()
    {
        return $this->empleado()->pluck('name')->first();
    }

    public function empleado(){
        return $this->belongsTo('App\Models\Empleados\Empleado', 'empleado_id');
    }


}