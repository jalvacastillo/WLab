<?php

namespace App\Models\Empleados\Planillas;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Models\Admin\Empresa;

class Planilla extends Model {

    protected $table = 'empresa_planillas';
    protected $fillable = array(
        'fecha_inicio',        
        'fecha_fin',        
        'fecha',        
        'total',
        'nota',
        'usuario_id',
        'empresa_id'
    );

    public $appends = ['nombre_usuario', 'sueldos','extras', 'otros', 'renta', 'isss', 'afp'];

    public function getNombreUsuarioAttribute(){
        return $this->usuario()->pluck('name')->first();
    }

    public function getSueldosAttribute(){
        return $this->detalles()->sum('sueldo');
    }

    public function getExtrasAttribute(){
        return $this->detalles()->sum('extras');
    }

    public function getOtrosAttribute(){
        return $this->detalles()->sum('otros');
    }

    public function getRentaAttribute(){
        return $this->detalles()->sum('renta');
    }

    public function getIsssAttribute(){
        return $this->detalles()->sum('isss');
    }

    public function getAfpAttribute(){
        return $this->detalles()->sum('afp');
    }

    public function detalles(){
        return $this->hasMany('App\Models\Empleados\Planillas\Detalle');
    }

    public function usuario(){
        return $this->belongsTo('App\Models\User', 'usuario_id');
    }

    public function empresa(){
        return $this->belongsTo('App\Models\Admin\Empresa', 'empresa_id');
    }


}



