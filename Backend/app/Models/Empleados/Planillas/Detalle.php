<?php

namespace App\Models\Empleados\Planillas;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Models\Admin\Empresa;

class Detalle extends Model {

    protected $table = 'empresa_planilla_detalles';
    protected $fillable = array(
        'empleado_id',
        'dias',
        'horas',
        'horas_extras',
        'sueldo',
        'extras',
        'otros',
        'renta',
        'isss',
        'afp',
        'total',
        'planilla_id'
    );

    public $appends = ['nombre_empleado'];

    public function getNombreEmpleadoAttribute(){
        return $this->empleado()->pluck('name')->first();
    }

    public function empleado(){
        return $this->belongsTo('App\Models\Empleados\Empleado', 'empleado_id');
    }

    public function planilla(){
        return $this->belongsTo('App\Models\Empleados\Planillas\Planilla', 'planilla_id');
    }


}



