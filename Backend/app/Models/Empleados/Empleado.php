<?php

namespace App\Models\Empleados;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Empleado extends Model {

    protected $table = 'users';
    protected $fillable = [
        'name',
        'email',
        'username',
        'password',
        'tipo',
        'telefono',
        'direccion',
        'dui',
        'nit',
        'nota',
        'avatar',
        'codigo',
        'activo',
        'empleado',
        'caja_id',
        'sucursal_id',
        'ultimo_login',
        'ultimo_logout',
    ];

    protected $dates = ['entrada', 'salida'];
    protected $hidden = ['password', 'remember_token'];
    protected $appends = ['salario','empresa_id','nombre_sucursal', 'dias_laborales', 'horas_extras', 'horas_laborales'];
    protected $casts = ['empleado' => 'boolean', 'activo' => 'boolean'];

    protected static function boot()
    {

        static::addGlobalScope('test', function ($query) {
            $query->where('empleado', true);
        });

        parent::boot();
    }

    public function getSalarioAttribute(){
        return $this->contrato()->pluck('sueldo')->first();
    }

    public function getHorasLaboralesAttribute(){
        return $this->asistencias()->whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'))->get()->sum('horas_laborales');
    }

    public function getHorasExtrasAttribute(){
        return $this->asistencias()->whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'))->get()->sum('horas_extras');
    }

    public function getDiasLaboralesAttribute(){
        return $this->asistencias()->whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'))->count();
    }

    public function getNombreSucursalAttribute(){
        return $this->sucursal()->pluck('nombre')->first();
    }

    public function getEmpresaIDAttribute(){
        return $this->sucursal()->first()->empresa()->pluck('id')->first();
    }

    public function sucursal(){
        return $this->belongsTo('App\Models\Admin\Sucursal', 'sucursal_id');
    }

    public function caja(){
        return $this->belongsTo('App\Models\Admin\Caja', 'caja_id');
    }

    public function contrato(){
        return $this->hasOne('App\Models\Empleados\Contrato');
    }

    public function cortes(){
        return $this->hasMany('App\Models\Admin\Corte', 'usuario_id');
    }

    public function ventas(){
        return $this->hasMany('App\Models\Ventas\Venta', 'usuario_id')->where('estado', 'Cobrada');
    }

    public function asistencias(){
        return $this->hasMany('App\Models\Empleados\Asistencia', 'usuario_id');
    }

    public function comisiones(){
        return $this->hasMany('App\Models\Empleados\Comision', 'empleado_id');
    }

    public function asistenciaDiaria(){
        return $this->hasOne('App\Models\Empleados\Asistencia', 'usuario_id')->whereBetween('entrada', [Carbon::today()->toDateString() . ' 00:00:00', Carbon::today()->toDateString() . ' 23:59:59']);
    }


    public function asistenciaMensual(){
        return $this->hasMany('App\Models\Empleados\Asistencia', 'usuario_id')->whereMonth('entrada', date('m'))->whereYear('entrada', date('Y'));
    }

    public function compras(){
        return $this->hasMany('App\Models\Compras\Compra', 'usuario_id');
    }


}
