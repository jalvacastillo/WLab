<?php

namespace App\Models\Pacientes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Examen extends Model {

    protected $table = 'paciente_examenes';
    protected $fillable = array(
        'fecha',
        'paciente_id',
        'examen_id',
        'medico',
        'estado',
        'nota',
        'usuario_id',
        'consulta_id',
        'sucursal_id'
    );

    protected $appends = ['nombre_paciente', 'nombre_usuario', 'nombre_examen'];

    public function getNombreExamenAttribute()
    {
        return $this->examen()->pluck('nombre')->first();
    }

    public function getNombrePacienteAttribute()
    {
        return $this->paciente()->pluck('nombre')->first();
    }

    public function getNombreUsuarioAttribute()
    {
        return $this->usuario()->pluck('name')->first();
    }

    public function paciente(){
        return $this->belongsTo('App\Models\Pacientes\Paciente', 'paciente_id');
    }

    public function examen(){
        return $this->belongsTo('App\Models\Pacientes\Consultas\Examenes\Examen', 'examen_id');
    }

    public function consulta(){
        return $this->belongsTo('App\Models\Pacientes\Consultas\Consulta', 'consulta_id');
    }

    public function usuario(){
        return $this->belongsTo('App\Models\User', 'usuario_id');
    }


}

