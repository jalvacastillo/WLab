<?php

namespace App\Models\Pacientes\Consultas\Examenes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Examen extends Model {

    protected $table = 'empresa_examenes';
    protected $fillable = array(
        'nombre',
        'especialidad',
        'descripcion',
        'etiquetas',
        'empresa_id',
    );

    protected $appends = ['valores'];

    public function getValoresAttribute(){
        return $this->valores()->pluck('valor');
    }

    public function valores(){
        return $this->hasMany('App\Models\Pacientes\Consultas\Examenes\Valor','examen_id');
    }


}

