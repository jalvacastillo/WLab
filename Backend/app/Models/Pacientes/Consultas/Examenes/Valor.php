<?php

namespace App\Models\Pacientes\Consultas\Examenes;

use Illuminate\Database\Eloquent\Model;

class Valor extends Model {

    protected $table = 'empresa_examen_valores';
    protected $fillable = array(
        'examen_id',
        'valor'
    );

    public function examen()
    {
        return $this->belongsTo('App\Models\Pacientes\Consultas\Examenes\Examen','examen_id');
    }
}
