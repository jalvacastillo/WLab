<?php

namespace App\Models\Pacientes\Consultas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Receta extends Model {

    protected $table = 'paciente_consulta_recetas';
    protected $fillable = array(
        'producto_id',
        'dosis',
        'cantidad',
        'consulta_id',
    );

    protected $appends = ['nombre_producto'];

    public function getNombreProductoAttribute()
    {
        return $this->producto()->first()->nombre;
    }

    public function consulta(){
        return $this->belongsTo('App\Models\Pacientes\Consultas\Consulta', 'consulta_id');
    }

    public function producto(){
        return $this->belongsTo('App\Models\Inventario\Producto', 'producto_id');
    }



}

