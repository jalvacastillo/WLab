<?php

namespace App\Models\Pacientes\Consultas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
class Consulta extends Model {

    protected $table = 'paciente_consultas';
    protected $fillable = array(
        'fecha',
        'paciente_id',
        'tipo',
        'estado',
        'motivo',
        'consulta',
        'diagnostico',
        'receta',
        'examen',
        'usuario_id',
        'sucursal_id'
    );

    protected $appends = ['nombre_paciente', 'nombre_usuario'];

    public function getFechaAttribute($value)
    {
        return Carbon::parse($value);
    }

    public function getNombrePacienteAttribute()
    {
        return $this->paciente()->pluck('nombre')->first();
    }

    public function getNombreUsuarioAttribute()
    {
        return $this->usuario()->pluck('name')->first();
    }

    public function signos(){
        return $this->hasMany('App\Models\Pacientes\Consultas\Signo');
    }

    public function recetas(){
        return $this->hasMany('App\Models\Pacientes\Consultas\Receta');
    }

    public function diagnosticos(){
        return $this->hasMany('App\Models\Pacientes\Consultas\Diagnostico');
    }

    public function examenes(){
        return $this->hasMany('App\Models\Pacientes\Examen');
    }

    public function paciente(){
        return $this->belongsTo('App\Models\Pacientes\Paciente', 'paciente_id');
    }

    public function sucursal(){
        return $this->belongsTo('App\Models\Admin\Sucursal', 'sucursal_id');
    }

    public function usuario(){
        return $this->belongsTo('App\Models\User', 'usuario_id');
    }



}

