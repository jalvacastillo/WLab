<?php

namespace App\Models\Pacientes\Consultas\Diagnosticos;

use Illuminate\Database\Eloquent\Model;

class Diagnostico extends Model {

    protected $table = 'empresa_diagnosticos';
    protected $fillable = array(
        'codigo',
        'descripcion',
        'equivalencias',
        'empresa_id'
    );


}



