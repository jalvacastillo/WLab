<?php

namespace App\Models\Pacientes\Consultas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Signo extends Model {

    protected $table = 'paciente_consulta_signos';
    protected $fillable = array(
        'fecha',
        'peso',
        'peso_unidad',
        'estatura',
        'estatura_unidad',
        'temperatura',
        'temperatura_unidad',
        'presion',
        'nota',
        'consulta_id',
        'usuario_id',
    );

    protected $appends = ['nombre_paciente', 'nombre_usuario'];

    public function getFechaAttribute($value)
    {
        return Carbon::parse($value);
    }

    public function getNombrePacienteAttribute()
    {
        return $this->paciente()->pluck('nombre')->first();
    }

    public function getNombreUsuarioAttribute()
    {
        return $this->usuario()->pluck('name')->first();
    }

    public function paciente(){
        return $this->belongsTo('App\Models\Pacientes\Paciente', 'paciente_id');
    }

    public function usuario(){
        return $this->belongsTo('App\Models\User', 'usuario_id');
    }



}

