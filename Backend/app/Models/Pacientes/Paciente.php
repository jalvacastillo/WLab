<?php

namespace App\Models\Pacientes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Paciente extends Model {

    protected $table = 'pacientes';
    protected $fillable = array(
        'fecha',
        'nombre',
        'dui',
        'nit',
        'fecha_nacimiento',
        'direccion',
        'municipio',
        'departamento',
        'telefono',
        'correo',
        'sexo',
        'profesion',
        'estado_civil',
        'citologia',
        'responsable',
        'madre',
        'padre',
        'alergias',
        'antecedentes',
        'medicacion_actual',
        'etiquetas',
        'img',
        'nota',
        'empresa_id'
    );

    protected $appends = ['edad'];
    // protected $dates = ['fecha', 'fecha_nacimiento'];

    public function getEdadAttribute(){
        if ($this->fecha_nacimiento)
            return Carbon::parse($this->fecha_nacimiento)->age;
        else
            return null;
    }

    public function consultas(){
        return $this->hasMany('App\Models\Pacientes\Consultas\Consulta', 'paciente_id');
    }


    public function examenes(){
        return $this->hasMany('App\Models\Pacientes\Examen', 'paciente_id');
    }

    public function citas(){
        return $this->hasMany('App\Models\Pacientes\Cita', 'paciente_id');
    }

    public function empresa() 
    {
        return $this->belongsTo('App\Models\Admin\Empresa', 'empresa_id');
    }


}

