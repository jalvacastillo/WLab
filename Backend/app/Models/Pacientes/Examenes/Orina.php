<?php

namespace App\Models\Pacientes\Examenes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orina extends Model {

    protected $table = 'examenes_orinas';
    protected $fillable = [
        'color',
        'aspecto',
        'densidad',
        'esterasa',
        'nitritos',
        'reaccion',
        'proteinas',
        'glucosa',
        'cetonicos',
        'urobitmogeno',
        'bilirubina',
        'sangre',
        'bacterias',
        'leucocitos',
        'hematies',
        'cilindros',
        'cristales',
        'celulas',
        'otros',
        'sucursal_id'
    ];

    public function examenes()
    {
        return $this->belongsTo('App\Models\Pacientes\Examenes','examen_id');
    }
}




