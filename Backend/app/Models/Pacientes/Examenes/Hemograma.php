<?php

namespace App\Models\Pacientes\Examenes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hemograma extends Model {

    use SoftDeletes;
    protected $table = 'examenes_hemogramas';
    protected $fillable = array(
        'globulos',
        'hematocritos',
        'hemoglobina',
        'volumen',
        'concentracion',
        'globular',
        'blancos',
        'basofitos',
        'eosinofilos',
        'neutrofilos',
        'linfocitos',
        'monocitos',
        'plaquetas',
        'observaciones',
        'sucursal_id'
    );

    public function examenes(){
        return $this->belongsTo('App\Models\Pacientes\Examenes', 'examen_id');
    }
}



