<?php

namespace App\Models\Pacientes\Examenes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuimicaDetalle extends Model {

    use SoftDeletes;
    protected $table = 'examenes_quimicas_detalle';
    protected $fillable = array(
        'quimica_id',
        'resultado',
        'examen',
        'valor'
    );


    public function examenes(){
        return $this->belongsTo('App\Models\Pacientes\Examenes', 'analisis_id');
    }

    public function resultados(){
        return $this->hasMany('App\Models\Pacientes\Examenes\QuimicaResultado', 'quimica_id');
    }
}

