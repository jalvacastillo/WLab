<?php

namespace App\Models\Pacientes\Examenes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Diverso extends Model {

    protected $table = 'examenes_diversos';
    protected $fillable = array(
        'muestra',
        'examen',
        'tipo',
        'resultado',
        'sucursal_id'
    );

    public function examenes(){
        return $this->belongsTo('App\Models\Pacientes\Examenes', 'examen_id');
    }
}


