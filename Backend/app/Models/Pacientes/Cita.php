<?php

namespace App\Models\Pacientes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cita extends Model {

    protected $table = 'paciente_citas';
    protected $fillable = array(
        'fecha',
        'tipo',
        'estado',
        'nota',
        'paciente_id',
        'medico_id',
        'usuario_id',
    );

    protected $appends = ['nombre_paciente', 'nombre_medico'];

    public function getNombrePacienteAttribute()
    {
        return $this->paciente()->pluck('nombre')->first();
    }
    public function getNombreMedicoAttribute()
    {
        return $this->medico()->pluck('name')->first();
    }

    public function paciente(){
        return $this->belongsTo('App\Models\Pacientes\Paciente', 'paciente_id');
    }

    public function medico(){
        return $this->belongsTo('App\Models\User', 'medico_id');
    }



}

