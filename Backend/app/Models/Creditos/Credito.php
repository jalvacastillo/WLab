<?php

namespace App\Models\Creditos;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Credito extends Model {
    
    protected $table = 'creditos';
    protected $fillable = [
        'fecha',
        'venta_id',
        'total',
        'interes',
        'plazo',
        'prima',
        'nota',
        'cliente_id',
        'usuario_id',
        'empresa_id'
    ];

    protected $appends = ['nombre_usuario', 'nombre_cliente', 'cuota', 'mora', 
                        'cantidad_de_pagos', 'saldo', 'fecha_de_pago', 'fecha_vencida',
                        'total_intereses', 'total_abonos', 'total_pagos'
                        ];

    public function getFechaAttribute($value)
    {
         return Carbon::parse($value)->format('Y-m-d');
    }

    public function getNombreUsuarioAttribute() 
    {
        return $this->usuario()->pluck('name')->first();
    }

    public function getNombreClienteAttribute() 
    {
        return $this->cliente()->pluck('nombre')->first();
    }

    public function getMoraAttribute(){

        $fechaLimite = \Carbon\Carbon::parse($this->fecha)->addMonths($this->plazo)->format('d/m/Y');
        if ($fechaLimite > date('Y-m-d')) {
            return true;
        }else{
            return false;
        }
    }

    public function getFechaDePagoAttribute(){
        if ($this->saldo > 0)
            $fecha = \Carbon\Carbon::parse($this->fecha)->addMonths($this->cantidad_de_pagos + 1)->format('d/m/Y');
        else
            $fecha = 'Completado';
        return $fecha;
    }

    public function getFechaVencidaAttribute(){
        if ($this->fecha_de_pago < date('d-m-Y'))
            return true;
        else
            return false;
    }

    public function getSaldoAttribute(){
        
        $descuentos = $this->pagos()->sum('descuento');
        return round($this->total - ($this->total_abonos + $this->prima + $descuentos), 4);

    }

    public function getTotalAbonosAttribute(){
        
        $pagos = $this->pagos()->sum('abono');
        return round($pagos, 4);

    }

    public function getTotalInteresesAttribute(){
        
        $intereses = $this->pagos()->sum('interes');
        return round($intereses, 4);

    }

    public function getTotalPagosAttribute(){
        
        $total_pagos = $this->total_abonos + $this->total_intereses;
        return round($total_pagos, 4);

    }

    public function getCantidadDePagosAttribute(){
        
        return $this->pagos()->count();

    }

    public function getCuotaAttribute(){
        $p = $this->total;
        $i = $this->interes;

        if ($i == 0)
            return $this->total / $this->plazo;

        if ($i > 1){
            $i = $i / 100;
        }
        $i /= 12;

        $n = 1;
        for ($j = 0; $j < $this->plazo; $j++){
            $n = $n * (1 + $i);
        }

        return (($p * $i * $n) / ($n - 1));
    
    }

    
    public function pagos() 
    {
        return $this->hasMany('App\Models\Creditos\Pago');
    }

    public function venta() 
    {
        return $this->belongsTo('App\Models\Ventas\Venta', 'venta_id');
    }

    public function usuario() 
    {
        return $this->belongsTo('App\Models\User', 'usuario_id');
    }

    public function cliente() 
    {
        return $this->belongsTo('App\Models\Ventas\Clientes\Cliente', 'cliente_id');
    }

    public function empresa() 
    {
        return $this->belongsTo('App\Models\Admin\Empresa', 'empresa_id');
    }

}
