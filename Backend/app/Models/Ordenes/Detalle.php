<?php

namespace App\Models\Ordenes;

use Illuminate\Database\Eloquent\Model;

class Detalle extends Model {
    
    protected $table = 'orden_detalles';
    protected $fillable = [
        'producto_id', 
        'estado', 
        'cantidad', 
        'precio', 
        'costo', 
        'descuento', 
        'total', 
        'nota', 
        'tiempo',
        'orden_id'
    ];

    protected $appends = ['nombre_producto', 'tipo_impuesto'];


    public function getTipoImpuestoAttribute() 
    {
        return $this->producto()->pluck('tipo_impuesto')->first();
    }

    public function getNombreProductoAttribute() 
    {
        return $this->producto()->pluck('nombre')->first();
    }

    public function producto() 
    {
        return $this->belongsTo('App\Models\Inventario\Producto', 'producto_id');
    }

    public function orden() 
    {
        return $this->belongsTo('App\Models\Ordenes\Orden', 'orden_id');
    }

    public function empresa() 
    {
        return $this->belongsTo('App\Empresa', 'empresa_id');
    }

}