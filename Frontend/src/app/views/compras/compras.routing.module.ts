import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from '../../layout/layout.component';

import { ComprasComponent } from '../../views/compras/compras.component';
import { CompraComponent } from '../../views/compras/compra/compra.component';
import { DevolucionesComprasComponent } from '../../views/compras/devoluciones/devoluciones-compras.component';
import { DevolucionCompraComponent } from '../../views/compras/devoluciones/devolucion/devolucion-compra.component';
import { ProveedoresComponent } from '../../views/compras/proveedores/proveedores.component';
import { ProveedorComponent } from '../../views/compras/proveedores/proveedor/proveedor.component';
import { CuentasPagarComponent } from '../../views/compras/proveedores/cuentas-pagar/cuentas-pagar.component';

import { HistorialComprasComponent } from '../../views/reportes/compras/historial/historial-compras.component';
import { DetalleComprasComponent } from '../../views/reportes/compras/detalle/detalle-compras.component';
import { CategoriasComprasComponent } from '../../views/reportes/compras/categorias/categorias-compras.component';


const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    title: 'Compras',
    children: [
        { path: 'compras', component: ComprasComponent },
        { path: 'compra/:id', component: CompraComponent },
        { path: 'devoluciones/compras', component: DevolucionesComprasComponent },
        { path: 'devolucion/compra/:id', component: DevolucionCompraComponent },
        { path: 'proveedores', component: ProveedoresComponent },
        { path: 'proveedores/cuentas-pagar', component: CuentasPagarComponent },
        { path: 'proveedor/:id', component: ProveedorComponent },
       
        { path: 'reporte/compras/historial', component: HistorialComprasComponent },
        { path: 'reporte/compras/detalle', component: DetalleComprasComponent },
        { path: 'reporte/compras/categorias', component: CategoriasComprasComponent },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComprasRoutingModule { }
