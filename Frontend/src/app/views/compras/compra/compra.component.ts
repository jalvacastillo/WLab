import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { SumPipe }     from '../../../pipes/sum.pipe';
import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';


@Component({
  selector: 'app-compra',
  templateUrl: './compra.component.html',
  providers: [ SumPipe ]
})

export class CompraComponent implements OnInit {

	public compra: any= {};
	public detalle: any = {};
	public detalleModificado: any = {};
    public loading = false;
    modalRef!: BsModalRef;
    
	constructor( 
	    private apiService: ApiService, private alertService: AlertService,
	    private route: ActivatedRoute, private router: Router,
	    private modalService: BsModalService, private sumPipe:SumPipe
	) { }

	ngOnInit() {

	    
	    const id = +this.route.snapshot.paramMap.get('id')!;
	        
        if(isNaN(id)){
            this.cargarDatosIniciales();
        }
        else{
            this.loading = true;
            this.cargarDatosIniciales();
            // Optenemos el compra
            this.apiService.read('compra/', id).subscribe(compra => {
               	this.compra = compra;
        		this.loading = false;
            });
        }

	}

	cargarDatosIniciales(){
		this.compra = {};
		this.compra.proveedor = {};
        this.compra.proveedor.empresa_id = this.apiService.auth_user().empresa_id;;
		this.compra.detalles = [];
        this.compra.fecha_pago = null;
		this.compra.fecha = this.apiService.date();
		this.compra.tipo = 'Interna';
        this.compra.estado = 'Pagada';
        this.compra.descuento = 0;
		this.detalle = {};
		this.sumTotal();
		// this.compra.interno = 0;
		this.compra.usuario_id = this.apiService.auth_user().id;
        this.compra.empresa_id = this.apiService.auth_user().empresa_id;
	}



    openModal(template: TemplateRef<any>) {
        this.compra.tipo = 'Interna';
        this.modalRef = this.modalService.show(template);
    }

    updateOrden(compra:any) {
        this.compra = compra;
        this.sumTotal();
    }


	public sumTotal() {
        this.compra.subtotal = this.sumPipe.transform(this.compra.detalles, 'subtotal') - this.compra.descuento;
        if(this.compra.retencion) { 
            this.compra.iva_retenido = parseFloat((this.compra.subtotal * 0.01).toFixed(2));
        }else{
            this.compra.iva_retenido = 0;            
        }
        this.compra.descuento = this.compra.descuento + this.sumPipe.transform(this.compra.detalles, 'descuento');
        this.compra.iva = this.compra.subtotal * 0.13;
        this.compra.total = this.compra.subtotal + this.compra.iva_retenido + this.compra.iva;
	}

	// Proveedor
	proveedorSelect(event:any):void{
        this.compra.proveedor = event.proveedor;
        if(this.compra.proveedor.tipo == "Grande") {
        	this.compra.retencion = 1;
        	this.sumTotal();
        }
    }

    // Producto o Gasolina
    productoSelect(event:any):void{
        this.detalle = Object.assign({}, event.detalle);
        this.detalle.id = null;
		this.compra.detalles.push(this.detalle);
		this.detalle = {};

		// Mantener el scroll hasta abajo en la lista de productos
		setTimeout(function(){
		    var objDiv = document.getElementById("detalles");
		    objDiv!.scrollTop = objDiv!.scrollHeight;
		},300);
		
		this.sumTotal();
    }

	public onSubmit() {

		this.loading = true;
		
        if(!this.compra.fecha_pago)
            this.compra.fecha_pago = this.apiService.date();

        if (!this.compra.proveedor.id) {
            this.compra.proveedor.empresa_id = this.apiService.auth_user().empresa_id;
        }


	    this.apiService.store('compra/facturacion', this.compra).subscribe(compra => {

	        if (this.modalRef) { this.modalRef.hide() }
	        this.loading = false;

            if (!this.compra.id)
    	        this.router.navigate(['/compras']);
	        this.alertService.success("Guardado");
            
	    },error => {this.alertService.error(error); this.loading = false; });


	}
    
    public openModalDevolucion(template: TemplateRef<any>) {
        this.compra.tipo = 'Cambio de producto';
        this.modalRef = this.modalService.show(template);
    }

	public onDevolucion(){
		this.apiService.store('devolucion/compra', this.compra).subscribe(compra => {
            if (this.modalRef) { this.modalRef.hide() }
            this.loading = false;
            this.cargarDatosIniciales()
            this.alertService.success("Guardado");
        },error => {this.alertService.error(error); this.loading = false; });
        
	}

	public onPosponer(){
		this.compra.estado = 'Pendiente';
		this.onSubmit();
	}


	public eliminarDetalle(detalle:any){
		if (confirm('¿Desea eliminar el Registro?')) {
			if(detalle.id) {
				this.apiService.delete('compra/detalle/', detalle.id).subscribe(detalle => {
					for (var i = 0; i < this.compra.detalles.length; ++i) {
						if (this.compra.detalles[i].id === detalle.id ){
							this.compra.detalles.splice(i, 1);
						}
					}
	        	}, error => {this.alertService.error(error._body); });
			}else{
				for (var i = 0; i < this.compra.detalles.length; ++i) {
					if (this.compra.detalles[i].producto_id === detalle.producto_id ){
						this.compra.detalles.splice(i, 1);
					}
				}
			}
			this.sumTotal();
		}
	}

	//Para Editar detalle

	public editarDetalle(template: TemplateRef<any>, detalle:any) {
        this.detalle = detalle;
        this.detalleModificado.tipo = this.detalle.tipo;
        this.detalleModificado.precio = this.detalle.precio;
        this.detalleModificado.precio_nuevo = this.detalle.precio_nuevo;
		this.modalRef = this.modalService.show(template, {class: 'modal-lg', backdrop: 'static'});
    }

    public guardarDetalle() {
    	this.detalle.cantidad = this.detalleModificado.cantidad;
    	this.detalle.costo = this.detalleModificado.costo;
    	this.detalle.costo_iva = this.detalleModificado.costo_iva;
    	this.detalle.cotrans = this.detalleModificado.cotrans;
    	this.detalle.fovial = this.detalleModificado.fovial;
    	this.detalle.iva = this.detalleModificado.iva;
    	this.detalle.precio = this.detalleModificado.precio;
    	this.detalle.precio_nuevo = this.detalleModificado.precio_nuevo;
    	this.detalle.tipo = this.detalleModificado.tipo;
    	this.detalle.total = this.detalleModificado.total;
    	this.detalle.subtotal = this.detalleModificado.subtotal;
    	console.log(this.detalle);
        this.modalRef.hide();
        this.sumTotal();
    }

	calcular(){

        if(this.detalleModificado.costo) {
            this.detalleModificado.costo_iva  = (parseFloat(this.detalleModificado.costo) + (this.detalleModificado.costo * 0.13)).toFixed(2);
        	if(!this.detalleModificado.subtotal){
                this.detalleModificado.subtotal = ((this.detalleModificado.cantidad * this.detalleModificado.costo) - this.detalleModificado.descuento).toFixed(2);
            }
        }
        
        this.detalleModificado.iva = 0;
        if(this.detalleModificado.tipo == 'Gravada'){
            this.detalleModificado.iva = (this.detalleModificado.subtotal * 0.13).toFixed(2);
        }
        
        this.detalleModificado.fovial = 0;
        this.detalleModificado.cotrans = 0;
        this.detalleModificado.total = (parseFloat(this.detalleModificado.subtotal) + parseFloat(this.detalleModificado.iva) + parseFloat(this.detalleModificado.fovial) + parseFloat(this.detalleModificado.cotrans)).toFixed(2);
  
    }
    
    calcularCosto(){
        this.detalleModificado.costo = (this.detalleModificado.subtotal / this.detalleModificado.cantidad).toFixed(4);
        this.calcular();
    }


}
