import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashSliderComponent } from './dash-slider.component';

describe('DashSliderComponent', () => {
  let component: DashSliderComponent;
  let fixture: ComponentFixture<DashSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
