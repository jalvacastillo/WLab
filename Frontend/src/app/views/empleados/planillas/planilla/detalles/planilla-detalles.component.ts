import { Component, OnInit, EventEmitter, Input, Output, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef} from 'ngx-bootstrap/modal';

import { ApiService } from '../../../../../services/api.service';
import { AlertService } from '../../../../../services/alert.service';

@Component({
  selector: 'app-planilla-detalles',
  templateUrl: './planilla-detalles.component.html'
})
export class PlanillaDetallesComponent implements OnInit {

    @Input() planilla: any = {};
    public detalle:any = {};
    public empleados:any = [];

    @Output() update = new EventEmitter();
    modalRef?: BsModalRef;

    public buscador:string = '';
    @Input() loading:boolean = false;

    constructor( 
        public apiService: ApiService, private alertService: AlertService,
        private modalService: BsModalService
    ) { }

    ngOnInit() {
        this.apiService.getAll('empleados').subscribe(empleados => {
            this.empleados = empleados;
            // this.generarPlanilla();
        }, error => {this.alertService.error(error); this.loading = false; });
    }
    public generarPlanilla(){
        this.loading = true;
        if (this.planilla.detalles.length === 0) {
                
            for (var i = 0; i < this.empleados.data.length; ++i) {
                this.detalle.empleado_id = this.empleados.data[i].id;
                this.detalle.nombre_empleado = this.empleados.data[i].name;
                this.detalle.planilla_id = this.planilla.id;


                this.detalle.dias = this.empleados.data[i].dias_laborales;
                this.detalle.horas = this.empleados.data[i].horas_laborales;
                this.detalle.horas_extras = this.empleados.data[i].horas_extras;

                if (this.empleados.data[i].contrato) {
                    this.detalle.sueldo = this.empleados.data[i].contrato.sueldo;
                    this.detalle.extras = 0;
                    this.detalle.otros = 0;
                    
                    this.detalle.renta = this.empleados.data[i].contrato.renta ? this.empleados.data[i].contrato.sueldo * 0.10: 0;
                    this.detalle.afp = this.empleados.data[i].contrato.afp ? this.empleados.data[i].contrato.sueldo * 0.0725: 0;
                    this.detalle.isss = this.empleados.data[i].contrato.isss ? this.empleados.data[i].contrato.sueldo * 0.03: 0;
                }else{
                    this.detalle.sueldo = 0;
                    this.detalle.extras = 0;
                    this.detalle.otros = 0;
                    
                    this.detalle.renta = 0;
                    this.detalle.afp = 0;
                    this.detalle.isss = 0;
                }
                this.detalle.total = parseFloat(this.detalle.sueldo) + parseFloat(this.detalle.extras) + parseFloat(this.detalle.otros) - parseFloat(this.detalle.renta) - parseFloat(this.detalle.afp) - parseFloat(this.detalle.isss);
                this.apiService.store('planilla/detalle', this.detalle).subscribe(detalle => {
                    this.planilla.detalles.push(detalle);
                    if (i == this.empleados.data.length) {
                        this.loading = false;
                        this.update.emit(this.planilla);
                    }
                },error => {this.alertService.error(error); this.loading = false; });
            }
        }
    }

    openModal(template: TemplateRef<any>, detalle:any) {
        this.detalle = detalle;
        this.modalRef = this.modalService.show(template, {backdrop: 'static'});
    }

    // Agregar detalle
    onSubmit():void{
        this.loading = true;
        this.detalle.total = parseFloat(this.detalle.sueldo) + parseFloat(this.detalle.extras) + parseFloat(this.detalle.otros) - parseFloat(this.detalle.renta) - parseFloat(this.detalle.afp) - parseFloat(this.detalle.isss);
        this.apiService.store('planilla/detalle', this.detalle).subscribe(detalle => {
            if (!this.detalle.id)
                this.planilla.detalles.push(this.detalle);
            this.update.emit(this.planilla);
            this.modalRef?.hide();
            this.alertService.success("Guardado");
            this.loading = false;
        },error => {this.alertService.error(error); this.loading = false; });

    }


    // Eliminar detalle
        public eliminarDetalle(detalle:any){
            if (confirm('Confirma que desea eliminar el elemento')) { 
                this.apiService.delete('planilla/detalle/', detalle.id).subscribe(detalle => {
                    for (var i = 0; i < this.planilla.detalles.length; ++i) {
                        if (this.planilla.detalles[i].id === detalle.id ){
                            this.planilla.detalles.splice(i, 1);
                            this.update.emit(this.planilla);
                        }
                    }
                },error => {this.alertService.error(error); this.loading = false; });
            }

        }


}
