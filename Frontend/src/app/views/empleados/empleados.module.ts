import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { FocusModule } from 'angular2-focus';
import { PipesModule } from '../../pipes/pipes.module';
import { SharedModule } from '../../shared/shared.module';

import { EmpleadosRoutingModule } from './empleados.routing.module';

import { EmpleadoComponent } from './empleado/empleado.component';
import { EmpleadosComponent } from './empleados.component';
import { EmpleadoContratoComponent } from './empleado/contrato/empleado-contrato.component';
import { PropinasComponent } from './propinas/propinas.component';
import { AsistenciasComponent } from './asistencias/asistencias.component';
import { AsistenciaComponent } from './asistencias/asistencia/asistencia.component';
import { PlanillasComponent } from './planillas/planillas.component';
import { PlanillaComponent } from './planillas/planilla/planilla.component';
import { PlanillaDetallesComponent } from './planillas/planilla/detalles/planilla-detalles.component';
import { ComisionesComponent } from './comisiones/comisiones.component';
import { ComisionComponent } from './comisiones/comision/comision.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    PipesModule,
    SharedModule,
    EmpleadosRoutingModule,
    TooltipModule.forRoot(),
    PopoverModule.forRoot(),
    FocusModule.forRoot()
  ],
  declarations: [
    EmpleadoComponent,
    EmpleadosComponent,
    EmpleadoContratoComponent,
    PropinasComponent,
    AsistenciasComponent,
    AsistenciaComponent,
    PlanillasComponent,
    PlanillaComponent,
    PlanillaDetallesComponent,
    ComisionesComponent,
    ComisionComponent
  ],
  exports: [
    EmpleadoComponent,
    EmpleadosComponent,
    EmpleadoContratoComponent,
    PropinasComponent,
    AsistenciasComponent,
    AsistenciaComponent,
    PlanillasComponent,
    PlanillaComponent,
    PlanillaDetallesComponent,
    ComisionesComponent,
    ComisionComponent
  ]
})
export class EmpleadosModule { }
