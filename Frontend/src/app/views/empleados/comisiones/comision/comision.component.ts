import { Component, OnInit, TemplateRef, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { AlertService } from '../../../../services/alert.service';
import { ApiService } from '../../../../services/api.service';

@Component({
  selector: 'app-comision',
  templateUrl: './comision.component.html'
})
export class ComisionComponent implements OnInit {

	@Output() loadAll = new EventEmitter();
	@Input() comision: any = {};
	public usuarios:any = [];
	public lugares:any [] = [];

 	public loading = false;

	modalRef!: BsModalRef;

   constructor(private apiService: ApiService, private alertService: AlertService,  
    	private route: ActivatedRoute, private router: Router,
    	private modalService: BsModalService
    ){ }

	ngOnInit() {
	}


	openModal(template: TemplateRef<any>) {
	    this.apiService.getAll('empleados').subscribe(usuarios => { 
	        this.usuarios = usuarios.data;
	    }, error => {this.alertService.error(error); });
	    if (!this.comision.id) {
			   this.comision = {};
			   this.comision.fecha = this.apiService.date();
			   this.comision.tipo = 'Venta';
			   this.comision.estado = 'Aprobada';
	    }
	   this.modalRef = this.modalService.show(template, {class: 'modal-md'});
	}
	
	public onSubmit() {
	   this.loading = true;
	   this.comision.usuario_id = this.apiService.auth_user().id;
	   this.apiService.store('comision', this.comision).subscribe(comision => {
	      this.modalRef.hide();
	      this.loadAll.emit();
	      this.loading = false;
	   }, error => {this.alertService.error(error); this.loading = false; });


	}

}
