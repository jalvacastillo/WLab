import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpleadoContratoComponent } from './empleado-contrato.component';

describe('EmpleadoContratoComponent', () => {
  let component: EmpleadoContratoComponent;
  let fixture: ComponentFixture<EmpleadoContratoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpleadoContratoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpleadoContratoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
