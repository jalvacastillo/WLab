import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html'
})
export class EmpleadoComponent implements OnInit {

	public empleado: any = {};
	public cajas: any = [];
	public sucursales: any = [];
  	public loading = false;

	constructor( 
	    private apiService: ApiService, private alertService: AlertService,
	    private route: ActivatedRoute, private router: Router
	) { }

	ngOnInit() {
		this.loadAll();
		
	    this.apiService.getAll('cajas').subscribe(cajas => { 
	        this.cajas = cajas;
	        this.loading = false;
	    }, error => {this.alertService.error(error); });

	    this.apiService.getAll('sucursales').subscribe(sucursales => { 
	        this.sucursales = sucursales;
	        this.loading = false;
	    }, error => {this.alertService.error(error); });
	}

	public loadAll() {
	    this.loading = true;
	    this.apiService.read('empleado/', +this.route.snapshot.paramMap.get('id')!).subscribe(empleado => { 
	        this.empleado = empleado;
	        if (!this.empleado.contrato) {
	        	this.empleado.contrato = {};
	        }
	        this.loading = false;
	    }, error => {this.alertService.error(error); this.loading = false;});

	}

	public onSubmit() {
	    this.loading = true;
	    if(this.empleado.tipo == 1) {
	    	this.empleado.caja_id == null;
	    }
	    // Guardamos al empleado
	    this.apiService.store('empleado', this.empleado).subscribe(empleado => {
	        this.empleado = empleado;
	        this.alertService.success("Guardado");
	        this.loading = false;
	    },error => {this.alertService.error(error); this.loading = false; });

	}

}
