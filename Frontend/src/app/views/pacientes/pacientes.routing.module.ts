import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from '../../layout/layout.component';

// Consultas
  import { ConsultasComponent } from './consultas/consultas.component';
  import { ConsultaComponent } from './consultas/consulta/consulta.component';

// Pacientes
import { PacientesComponent } from './pacientes.component';
import { PacienteComponent } from './paciente/paciente.component';

// Citas
import { CitasComponent } from './citas/citas.component';
// import { CitaComponent } from './citas/cita/cita.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
        { path: 'consultas', component: ConsultasComponent, title: 'Consultas' },
        { path: 'consulta/:id', component: ConsultaComponent, title: 'Consulta' },

        { path: 'pacientes', component: PacientesComponent, title: 'Pacientes' },
        { path: 'paciente/:id', component: PacienteComponent, title: 'Paciente' },

        { path: 'citas', component: CitasComponent, title: 'Citas' },
        // { path: 'cita/:id', component: CitaComponent, title: 'Cita' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PacientesRoutingModule { }
