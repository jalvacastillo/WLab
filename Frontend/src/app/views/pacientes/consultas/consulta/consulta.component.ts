import { Component, OnInit,TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { SumPipe }     from '../../../../pipes/sum.pipe';

import { AlertService } from '../../../../services/alert.service';
import { ApiService } from '../../../../services/api.service';
import * as moment from 'moment';

@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html'
})
export class ConsultaComponent implements OnInit {

    public consulta:any = {};
    public detalle:any = {};
    public consulta_id:number = 0;

    public paciente:any = {};

    public loading = false;

    constructor( public apiService:ApiService, private alertService:AlertService, private sumPipe:SumPipe,
        private route: ActivatedRoute, private router: Router, private modalService: BsModalService,
    ) {
        this.router.routeReuseStrategy.shouldReuseRoute = function() {return false; };
    }

      ngOnInit() {

        this.consulta_id = +this.route.snapshot.paramMap.get('id')!;
      
        if(isNaN(this.consulta_id)){
            this.consulta = {};
            this.consulta.paciente = {};
            this.consulta.paciente.nombre = '';
            this.cargarDatosIniciales();
        }
        else{
            this.consulta.paciente = {};
            this.consulta.paciente.nombre = '';
            this.loadAll();
        }

        // this.apiService.getAll('mesas').subscribe(mesas => {
        //    this.mesas = mesas;
        // }, error => {this.alertService.error(error); this.loading = false;});

    }

    cargarDatosIniciales(){
        this.consulta = {};
        this.consulta.fecha = moment(new Date()).format('YYYY-MM-DDTHH:mm');
        this.consulta.estado = 'En Espera';
        this.consulta.paciente = {};
        this.consulta.signos = [];
        this.consulta.tipo = 'En Clínica';
        this.detalle = {};
        this.consulta.usuario_id = this.apiService.auth_user().id;
        this.consulta.sucursal_id = this.apiService.auth_user().sucursal_id;
    }

    public loadAll(){
        this.loading = true;
        this.apiService.read('consulta/', this.consulta_id).subscribe(consulta => {
        this.consulta = consulta;
        this.consulta.fecha = moment(consulta.fecha).format('YYYY-MM-DDTHH:mm');
        if(consulta.paciente_id) {
            this.paciente.id = consulta.paciente_id;
        }
        this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});

    }

    updateOrden(consulta:any) {
        this.consulta = consulta;
    }

    public pacienteSelect(paciente:any){
        this.consulta.paciente = paciente;
    }


    // Agregar detalle
        examenSelect(examen:any):void{
            this.detalle = Object.assign({}, examen);
            this.detalle.id = null;
            
            // Verifica si el examen ya fue ingresado
            let detalle = this.consulta.detalles.find((x:any) => x.examen_id == this.detalle.examen_id);
            
            if(detalle) {
                this.detalle = detalle;
                this.detalle.cantidad += 1;
            }
            
            this.detalle.total = ((this.detalle.precio - this.detalle.descuento) * this.detalle.cantidad);
            
            if(!detalle)
                this.consulta.detalles.push(this.detalle);

            console.log(this.consulta);
            
            this.detalle = {};
            this.alertService.success("Agregado");

        }

    // Guardar consulta
        public onSubmit() {

            this.loading = true;
            
            if(!this.consulta.paciente.id && !this.consulta.paciente.nombre) {
                this.consulta.paciente.id = 1;
            }

            this.apiService.store('consulta', this.consulta).subscribe(consulta => {
                this.loading = false;
                if(this.consulta.id && this.consulta.estado != 'Finalizada') {
                   this.loadAll();
                }else{
                   this.router.navigate(['consulta/' + consulta.id]);
                }
                this.alertService.success("Guardado");
            },error => {this.alertService.error(error); this.loading = false; });

        }


        imprimirReceta(){
             window.open(this.apiService.baseUrl + '/api/imprimir/receta/' + this.consulta.id + '?token=' + this.apiService.auth_token(), 'hola', 'width=400');
        }

        imprimirExamenes(){
             window.open(this.apiService.baseUrl + '/api/imprimir/examenes/' + this.consulta.id + '?token=' + this.apiService.auth_token(), 'hola', 'width=400');
        }

}
