import { Component, OnInit, EventEmitter, Input, Output, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { ApiService } from '../../../../../services/api.service';
import { AlertService } from '../../../../../services/alert.service';

import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { fromEvent, timer } from 'rxjs';

@Component({
  selector: 'app-consulta-examenes',
  templateUrl: './consulta-examenes.component.html'
})
export class ConsultaExamenesComponent implements OnInit {

    @Input() consulta: any = {};
    @Output() update = new EventEmitter();
    modalRef!: BsModalRef;

    public examenes:any = [];
    public examen:any = {};
    public buscador:any = '';
    public loading:boolean = false;

    constructor( 
        private apiService: ApiService, private alertService: AlertService,
        private modalService: BsModalService
    ) { }

    ngOnInit() {
    }

    openModal(template: TemplateRef<any>) {
        this.examenes.total = 0;
        this.modalRef = this.modalService.show(template, { class: 'modal-lg', backdrop: 'static'});
        const input = document.getElementById('example')!;
        const example = fromEvent(input, 'keyup').pipe(map(i => (<HTMLTextAreaElement>i.currentTarget).value));
        const debouncedInput = example.pipe(debounceTime(500));
        const subscribe = debouncedInput.subscribe(val => { this.searchProducto(); });
    }

    searchProducto(){
        if(this.buscador && this.buscador.length > 2) {
            this.loading = true;
            this.apiService.read('examenes/buscar/', this.buscador).subscribe(examenes => {
               this.examenes = examenes.data;
               this.loading = false;
            }, error => {this.alertService.error(error);this.loading = false;});
        }else if (!this.buscador  || this.buscador.length < 1 ){ this.loading = false; this.buscador = ''; this.examenes.total = 0; }
    }


    public onSubmit(examen:any):void{
        this.examen.examen_id = examen.id;
        this.examen.consulta_id = this.consulta.id;
        this.examen.fecha = this.apiService.date();
        this.examen.estado = "Pendiente";
        this.examen.paciente_id = this.consulta.paciente_id;
        this.examen.usuario_id = this.apiService.auth_user().id;
        this.examen.sucursal_id = this.apiService.auth_user().sucursal_id;

        this.loading = true;
        this.apiService.store('consulta/examen', this.examen).subscribe(examen => {
            this.loading = false;
            if (!this.examen.id) {
                this.consulta.examenes.unshift(examen);
            }
            this.update.emit(this.consulta);
            this.alertService.success('Guardado');
            this.modalRef!.hide();
        },error => {this.alertService.error(error); this.loading = false; });
    }

    // Eliminar examen
    public delete(examen:any){
        if (confirm('Confirma que desea eliminar el elemento')) { 
            this.apiService.delete('consulta/examen/', examen.id).subscribe(examen => {
                for (var i = 0; i < this.consulta.examenes.length; ++i) {
                    if (this.consulta.examenes[i].id === examen.id ){
                        this.consulta.examenes.splice(i, 1);
                        this.update.emit(this.consulta);
                    }
                }
            },error => {this.alertService.error(error); this.loading = false; });
        }

    }


}
