import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultaExamenesComponent } from './consulta-examenes.component';

describe('ConsultaExamenesComponent', () => {
  let component: ConsultaExamenesComponent;
  let fixture: ComponentFixture<ConsultaExamenesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultaExamenesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultaExamenesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
