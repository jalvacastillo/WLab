import { Component, OnInit, EventEmitter, Input, Output, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef} from 'ngx-bootstrap/modal';

import { ApiService } from '../../../../../services/api.service';
import { AlertService } from '../../../../../services/alert.service';
import * as moment from 'moment';
@Component({
  selector: 'app-consulta-signos',
  templateUrl: './consulta-signos.component.html'
})
export class ConsultaSignosComponent implements OnInit {

    @Input() consulta: any = {};
    public signo:any = {};

    @Output() update = new EventEmitter();
    modalRef?: BsModalRef;

    public buscador:string = '';
    public loading:boolean = false;

    constructor( 
        public apiService: ApiService, private alertService: AlertService,
        private modalService: BsModalService
    ) { }

    ngOnInit() {

    }

    openModal(template: TemplateRef<any>, signo:any) {
        this.signo = signo;
        if (!signo.id){
            this.signo.fecha = moment(new Date()).format('YYYY-MM-DDTHH:mm');
            this.signo.peso_unidad = 'Lb';
            this.signo.estatura_unidad = 'Cm';
            this.signo.temperatura_unidad = 'Oral';
            this.signo.consulta_id = this.consulta.id;
            this.signo.usuario_id = this.apiService.auth_user().id;
        }
        else{
            this.signo.fecha = moment(signo.fecha).format('YYYY-MM-DDTHH:mm');
        }
        
        this.modalRef = this.modalService.show(template, { class: 'modal-md', backdrop: 'static' });
    }

    public onSubmit():void{
        this.loading = true;
        this.apiService.store('consulta/signo', this.signo).subscribe(signo => {
            this.loading = false;
            if (!this.signo.id) {
                this.consulta.signos.unshift(signo);
            }
            this.update.emit(this.consulta);
            this.alertService.success('Guardado');
            this.modalRef!.hide();
        },error => {this.alertService.error(error); this.loading = false; });
    }


    // Eliminar signo
    public eliminarDetalle(signo:any){
        if (confirm('Confirma que desea eliminar el elemento')) { 
            this.apiService.delete('consulta/signo/', signo.id).subscribe(signo => {
                for (var i = 0; i < this.consulta.signos.length; ++i) {
                    if (this.consulta.signos[i].id === signo.id ){
                        this.consulta.signos.splice(i, 1);
                        this.update.emit(this.consulta);
                    }
                }
            },error => {this.alertService.error(error); this.loading = false; });
        }

    }


}
