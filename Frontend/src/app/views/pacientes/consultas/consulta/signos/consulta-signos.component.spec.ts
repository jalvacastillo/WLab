import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultaSignosComponent } from './consulta-signos.component';

describe('ConsultaSignosComponent', () => {
  let component: ConsultaSignosComponent;
  let fixture: ComponentFixture<ConsultaSignosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultaSignosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultaSignosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
