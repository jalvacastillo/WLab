import { Component, OnInit, EventEmitter, Input, Output, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { fromEvent, timer } from 'rxjs';

import { ApiService } from '../../../../../services/api.service';
import { AlertService } from '../../../../../services/alert.service';

@Component({
  selector: 'app-consulta-paciente',
  templateUrl: './consulta-paciente.component.html'
})
export class ConsultaPacienteComponent implements OnInit {

    @Input() paciente: any = {};
    @Output() pacienteSelect = new EventEmitter();
    public pacientes: any = [];
    public searching = false;
    modalRef!: BsModalRef;

    public pagoAnticipado:boolean = false;

    constructor( 
        private apiService: ApiService, private alertService: AlertService,
        private modalService: BsModalService
    ) { }

    ngOnInit() {
    }

    openModal(template: TemplateRef<any>) {
        if(this.paciente.id) {
            this.searching = false;
        }
        this.modalRef = this.modalService.show(template, { backdrop: 'static' });

        setTimeout(()=>{
            const input = document.getElementById('example')!;
            const example = fromEvent(input, 'keyup').pipe(map(i => (<HTMLTextAreaElement>i.currentTarget).value));
            const debouncedInput = example.pipe(debounceTime(500));
            const subscribe = debouncedInput.subscribe(val => { this.searchPaciente(); });
        },500)
        
    }


    searchPaciente(){
        if(this.paciente.nombre && this.paciente.nombre.length > 1) {
            this.searching = true;
            this.apiService.read('pacientes/buscar/', this.paciente.nombre).subscribe(pacientes => {
               this.pacientes = pacientes;
               // if(pacientes.total) {
               //     if(pacientes.total == 1 && ((pacientes.data[0].nombre.replace('-', '')) == this.paciente.nombre)) {
               //         this.selectPaciente(pacientes.data[0]);
               //     }
               // }
               this.searching = false;
            }, error => {this.alertService.error(error);this.searching = false;});
        }else if (!this.paciente.nombre  || this.paciente.nombre.length < 1 ){ this.searching = false; this.paciente = {}; this.pacientes.total = 0; }
    }

    public selectPaciente(paciente:any){
        this.pacientes = [];
        this.paciente = paciente;
    }


    public onSubmit(paciente:any){
        this.pacienteSelect.emit(this.paciente);
        this.modalRef.hide()
    }

    clear(){
        if(this.pacientes.data && this.pacientes.data.length == 0) { this.pacientes = []; }
    }

}
