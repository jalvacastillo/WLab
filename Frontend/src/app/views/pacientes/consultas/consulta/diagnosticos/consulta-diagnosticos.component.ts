import { Component, OnInit, EventEmitter, Input, Output, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { ApiService } from '../../../../../services/api.service';
import { AlertService } from '../../../../../services/alert.service';

import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { fromEvent, timer } from 'rxjs';

@Component({
  selector: 'app-consulta-diagnosticos',
  templateUrl: './consulta-diagnosticos.component.html'
})
export class ConsultaDiagnosticosComponent implements OnInit {

    @Input() consulta: any = {};
    @Output() update = new EventEmitter();
    modalRef!: BsModalRef;

    public diagnosticos:any = [];
    public diagnostico:any = {};
    public buscador:any = '';
    public loading:boolean = false;

    constructor( 
        private apiService: ApiService, private alertService: AlertService,
        private modalService: BsModalService
    ) { }

    ngOnInit() {
    }

    openModal(template: TemplateRef<any>) {
        this.diagnosticos.total = 0;
        this.modalRef = this.modalService.show(template, { class: 'modal-lg', backdrop: 'static'});
        const input = document.getElementById('example')!;
        const example = fromEvent(input, 'keyup').pipe(map(i => (<HTMLTextAreaElement>i.currentTarget).value));
        const debouncedInput = example.pipe(debounceTime(500));
        const subscribe = debouncedInput.subscribe(val => { this.searchProducto(); });
    }

    searchProducto(){
        if(this.buscador && this.buscador.length > 2) {
            this.loading = true;
            this.apiService.read('diagnosticos/buscar/', this.buscador).subscribe(diagnosticos => {
               this.diagnosticos = diagnosticos.data;
               this.loading = false;
            }, error => {this.alertService.error(error);this.loading = false;});
        }else if (!this.buscador  || this.buscador.length < 1 ){ this.loading = false; this.buscador = ''; this.diagnosticos.total = 0; }
    }


    public onSubmit(diagnostico:any):void{
        diagnostico.id = null;
        diagnostico.consulta_id = this.consulta.id;
        this.loading = true;
        this.apiService.store('consulta/diagnostico', diagnostico).subscribe(diagnostico => {
            this.loading = false;
            if (!this.diagnostico.id) {
                this.consulta.diagnosticos.unshift(diagnostico);
            }
            this.update.emit(this.consulta);
            this.alertService.success('Guardado');
            this.modalRef!.hide();
        },error => {this.alertService.error(error); this.loading = false; });
    }

    // Eliminar diagnostico
    public delete(diagnostico:any){
        if (confirm('Confirma que desea eliminar el elemento')) { 
            this.apiService.delete('consulta/diagnostico/', diagnostico.id).subscribe(diagnostico => {
                for (var i = 0; i < this.consulta.diagnosticos.length; ++i) {
                    if (this.consulta.diagnosticos[i].id === diagnostico.id ){
                        this.consulta.diagnosticos.splice(i, 1);
                        this.update.emit(this.consulta);
                    }
                }
            },error => {this.alertService.error(error); this.loading = false; });
        }

    }


}
