import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultaDiagnosticosComponent } from './consulta-diagnosticos.component';

describe('ConsultaDiagnosticosComponent', () => {
  let component: ConsultaDiagnosticosComponent;
  let fixture: ComponentFixture<ConsultaDiagnosticosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultaDiagnosticosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultaDiagnosticosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
