import { Component, OnInit, EventEmitter, Input, Output, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { ApiService } from '../../../../../services/api.service';
import { AlertService } from '../../../../../services/alert.service';

import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { fromEvent, timer } from 'rxjs';

@Component({
  selector: 'app-consulta-recetas',
  templateUrl: './consulta-recetas.component.html'
})
export class ConsultaRecetasComponent implements OnInit {

    @Input() consulta: any = {};
    @Output() update = new EventEmitter();
    modalRef!: BsModalRef;

    public receta:any = {};
    public buscador:any = '';
    public loading:boolean = false;

    constructor( 
        private apiService: ApiService, private alertService: AlertService,
        private modalService: BsModalService
    ) { }

    ngOnInit() {
    }

    openModal(template: TemplateRef<any>) {
        this.receta.cantidad = 1;
        this.modalRef = this.modalService.show(template, { class: 'modal-lg', backdrop: 'static'});
    }


    selectProducto(producto:any){
        this.receta.cantidad = 1;
        this.receta.producto_id = producto.id;
        this.receta.consulta_id = this.consulta.id;
    }


    public onSubmit():void{
        this.loading = true;
        this.apiService.store('consulta/receta', this.receta).subscribe(receta => {
            this.loading = false;
            if (!this.receta.id) {
                this.consulta.recetas.push(receta);
            }
            this.update.emit(this.consulta);
            this.alertService.success('Guardado');
            this.receta = {};
            this.modalRef!.hide();
        },error => {this.alertService.error(error); this.loading = false; });
    }

    // Eliminar receta
    public delete(receta:any){
        if (confirm('Confirma que desea eliminar el elemento')) { 
            this.apiService.delete('consulta/receta/', receta.id).subscribe(receta => {
                for (var i = 0; i < this.consulta.recetas.length; ++i) {
                    if (this.consulta.recetas[i].id === receta.id ){
                        this.consulta.recetas.splice(i, 1);
                        this.update.emit(this.consulta);
                    }
                }
            },error => {this.alertService.error(error); this.loading = false; });
        }

    }


}
