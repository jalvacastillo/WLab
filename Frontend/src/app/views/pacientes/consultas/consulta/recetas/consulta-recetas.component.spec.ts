import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultaRecetasComponent } from './consulta-recetas.component';

describe('ConsultaRecetasComponent', () => {
  let component: ConsultaRecetasComponent;
  let fixture: ComponentFixture<ConsultaRecetasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultaRecetasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultaRecetasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
