import { Component, OnInit,TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-paciente',
  templateUrl: './paciente.component.html'
})
export class PacienteComponent implements OnInit {

    public paciente:any = {};

    public loading = false;
    modalRef?: BsModalRef;

    // Img Upload
    public file?:File;
    public preview = false;
    public url_img_preview:string = '';

	constructor( 
	    private apiService: ApiService, private alertService: AlertService,
	    private route: ActivatedRoute, private router: Router, private modalService: BsModalService
	) { }

	ngOnInit() {
        const id = +this.route.snapshot.paramMap.get('id')!;
        if(isNaN(id))
            this.paciente = {};
        else
            this.loadAll(id);

    }

    public loadAll(id:number){
        this.loading = true;
        this.apiService.read('paciente/', id).subscribe(paciente => {
            this.paciente = paciente;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});
    }

    public submit():void{
        this.loading = true;
        
        this.paciente.fecha = this.apiService.date();
        this.paciente.empresa_id = this.apiService.auth_user().empresa_id;

        let formData:FormData = new FormData();
        for (var key in this.paciente) {
            if (key == 'activo' || key == 'empleado') {
                this.paciente[key] = this.paciente[key] ? 1 : 0;
            }
            formData.append(key, this.paciente[key] == null ? '' : this.paciente[key]);
        }

        this.apiService.store('paciente', formData).subscribe(paciente => { 
            if(!this.paciente.id) {
               this.router.navigate(['/paciente/'+   paciente.id]);
            }
            this.paciente = paciente;
            this.loading = false;
            this.alertService.success('Guardado');
        }, error => {this.alertService.error(error); this.loading = false;});
    }

    setFile(event:any){
        this.file = event.target.files[0];
        this.paciente.file = this.file;
        var reader = new FileReader();
        reader.onload = ()=> {
            var url:any;
            url = reader.result;
            this.url_img_preview = url;
            this.preview = true;
           };
        reader.readAsDataURL(this.file!);
    }

    expediente(){
         window.open(this.apiService.baseUrl + '/api/paciente/expediente/' + this.paciente.id + '?token=' + this.apiService.auth_token(), 'hola', 'width=400');
    }


}
