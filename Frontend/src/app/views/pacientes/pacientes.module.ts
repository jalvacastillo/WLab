import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FocusModule } from 'angular2-focus';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { TagInputModule } from 'ngx-chips';
import { PipesModule } from '../../pipes/pipes.module';
import { SharedModule } from '../../shared/shared.module';
import { FullCalendarModule } from '@fullcalendar/angular';

import { PacientesRoutingModule } from './pacientes.routing.module';

import { PacientesComponent } from './pacientes.component';
import { PacienteComponent } from './paciente/paciente.component';

import { ConsultasComponent } from './consultas/consultas.component';
import { ConsultaComponent } from './consultas/consulta/consulta.component';
import { CitasComponent } from './citas/citas.component';
// import { CitaComponent } from './citas/cita/cita.component';
import { ConsultaPacienteComponent } from './consultas/consulta/paciente/consulta-paciente.component';
import { ConsultaExamenesComponent } from './consultas/consulta/examenes/consulta-examenes.component';
import { ConsultaDiagnosticosComponent } from './consultas/consulta/diagnosticos/consulta-diagnosticos.component';
import { ConsultaRecetasComponent } from './consultas/consulta/recetas/consulta-recetas.component';
import { ConsultaSignosComponent } from './consultas/consulta/signos/consulta-signos.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    SharedModule,
    TagInputModule,
    PipesModule,
    PacientesRoutingModule,
    FullCalendarModule,
    FocusModule.forRoot(),
    PopoverModule.forRoot(),
    TooltipModule.forRoot()
  ],
  declarations: [
    PacientesComponent,
  	PacienteComponent,
    ConsultasComponent,
    CitasComponent,
    ConsultaComponent,
    ConsultaPacienteComponent,
    ConsultaExamenesComponent,
    ConsultaDiagnosticosComponent,
    ConsultaRecetasComponent,
    ConsultaSignosComponent,
  ],
  exports: [
    PacientesComponent,
  	PacienteComponent,
    ConsultasComponent,
    CitasComponent,
    ConsultaComponent,
    ConsultaPacienteComponent,
    ConsultaExamenesComponent,
    ConsultaDiagnosticosComponent,
    ConsultaRecetasComponent,
    ConsultaSignosComponent,
  ]
})
export class PacientesModule { }
