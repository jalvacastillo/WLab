import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from '../../layout/layout.component';

import { OrdenesComponent } from '../../views/ordenes/ordenes.component';
import { OrdenComponent } from '../../views/ordenes/orden/orden.component';
import { VentasComponent } from '../../views/ventas/ventas.component';
import { VentaComponent } from '../../views/ventas/venta/venta.component';
import { DevolucionesVentasComponent } from '../../views/ventas/devoluciones/devoluciones-ventas.component';
import { DevolucionVentaComponent } from '../../views/ventas/devoluciones/devolucion/devolucion-venta.component';
import { FacturacionComponent } from '../../views/facturacion/facturacion.component';

import { ClientesComponent } from '../../views/ventas/clientes/clientes.component';
import { CuentasCobrarComponent } from '../../views/ventas/clientes/cuentas-cobrar/cuentas-cobrar.component';
import { ClienteComponent } from '../../views/ventas/clientes/cliente/cliente.component';
import { ClientesDashComponent } from '../../views/ventas/clientes/dash/clientes-dash.component';

import { HistorialVentasComponent } from '../../views/reportes/ventas/historial/historial-ventas.component';
import { DetalleVentasComponent } from '../../views/reportes/ventas/detalle/detalle-ventas.component';
import { CategoriasVentasComponent } from '../../views/reportes/ventas/categorias/categorias-ventas.component';


const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    title: 'Ventas',
    children: [
        { path: 'facturacion', component: FacturacionComponent },
        { path: 'ordenes', component: OrdenesComponent },
        { path: 'orden/:id', component: OrdenComponent },
        { path: 'ventas', component: VentasComponent },
        { path: 'venta/:id', component: VentaComponent },
        { path: 'devoluciones/ventas', component: DevolucionesVentasComponent },
        { path: 'devolucion/venta/:id', component: DevolucionVentaComponent },
    // Clientes
        { path: 'clientes', component: ClientesComponent },
        { path: 'clientes/cuentas-cobrar', component: CuentasCobrarComponent },
        { path: 'cliente/:id', component: ClienteComponent },
        { path: 'clientes/crm', component: ClientesDashComponent },

    // Reportes 
        { path: 'reporte/ventas/historial', component: HistorialVentasComponent },
        { path: 'reporte/ventas/detalle', component: DetalleVentasComponent },
        { path: 'reporte/ventas/categorias', component: CategoriasVentasComponent },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VentasRoutingModule { }
