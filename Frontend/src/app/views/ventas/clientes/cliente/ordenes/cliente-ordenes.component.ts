import { Component, OnInit,TemplateRef, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { BsModalService, BsModalRef} from 'ngx-bootstrap/modal';

import { AlertService } from '../../../../../services/alert.service';
import { ApiService } from '../../../../../services/api.service';

@Component({
  selector: 'app-cliente-ordenes',
  templateUrl: './cliente-ordenes.component.html'
})
export class ClienteOrdenesComponent implements OnInit {

    public ordenes:any = [];
    public orden:any = {};
    public countries:any = [];

    public loading = false;
    modalRef?: BsModalRef;

	constructor( 
	    private apiService: ApiService, private alertService: AlertService,
	    private route: ActivatedRoute, private router: Router, private modalService: BsModalService
	) { }

	ngOnInit() {
        const id = +this.route.snapshot.paramMap.get('id')!;
        if(isNaN(id))
            this.ordenes = [];
        else
            this.loadAll(id);
    }

    public loadAll(id:number){
        this.loading = true;
        this.apiService.getAll('cliente/' + id + '/ordenes').subscribe(ordenes => {
            this.ordenes = ordenes;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});

    }

    public setPagination(event:any):void{
        this.loading = true;
        this.apiService.paginate(this.ordenes.path + '?page='+ event.page).subscribe(ordenes => { 
            this.ordenes = ordenes;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});
    }


}
