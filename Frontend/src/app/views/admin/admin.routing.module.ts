import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../guards/auth.guard';
import { LayoutComponent } from '../../layout/layout.component';

import { EmpresaComponent }     from '../../views/admin/empresa/empresa.component';
import { CategoriasComponent } from '../../views/admin/categorias/categorias.component';


import { SucursalesComponent }     from '../../views/admin/sucursales/sucursales.component';
import { SucursalComponent }     from '../../views/admin/sucursales/sucursal/sucursal.component';
import { MesasComponent } from '../../views/admin/sucursales/mesas/mesas.component';
import { BodegasComponent } from '../../views/admin/sucursales/bodegas/bodegas.component';
import { UsuariosComponent }     from '../../views/admin/sucursales/usuarios/usuarios.component';
import { UsuarioComponent }     from '../../views/admin/sucursales/usuarios/usuario/usuario.component';
import { CajasComponent }     from '../../views/admin/sucursales/cajas/cajas.component';
import { CajaComponent }     from '../../views/admin/sucursales/cajas/caja/caja.component';

const routes: Routes = [
  {
    path: 'admin',
    component: LayoutComponent,
    children: [
        { path: 'negocio', component: EmpresaComponent },
        { path: 'sucursales', component: SucursalesComponent },
        { path: 'sucursal/:id', component: SucursalComponent },
        { path: 'cajas', component: CajasComponent },
        { path: 'caja/:id', component: CajaComponent },
        { path: 'bodegas', component: BodegasComponent },
        { path: 'mesas', component: MesasComponent },
        { path: 'usuarios', component: UsuariosComponent },
        { path: 'usuario/:id', component: UsuarioComponent },
        { path: 'categorias', component: CategoriasComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
