import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { FocusModule } from 'angular2-focus';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TagInputModule } from 'ngx-chips';

import { PipesModule } from '../../pipes/pipes.module';
import { SharedModule } from '../../shared/shared.module';
import { AdminRoutingModule } from './admin.routing.module';

import { EmpresaComponent } from './empresa/empresa.component';
import { SucursalesComponent } from './sucursales/sucursales.component';
import { SucursalComponent } from './sucursales/sucursal/sucursal.component';
import { SucursalUsuariosComponent } from './sucursales/sucursal/usuarios/sucursal-usuarios.component';
import { UsuariosComponent } from './sucursales/usuarios/usuarios.component';
import { UsuarioComponent } from './sucursales/usuarios/usuario/usuario.component';
import { MesasComponent } from './sucursales/mesas/mesas.component';
import { BodegasComponent } from './sucursales/bodegas/bodegas.component';
import { CajasComponent } from './sucursales/cajas/cajas.component';
import { DepartamentosComponent } from './sucursales/departamentos/departamentos.component';
import { CajaComponent } from './sucursales/cajas/caja/caja.component';
import { CajaCortesComponent } from './sucursales/cajas/caja/cortes/caja-cortes.component';
import { CajaUsuariosComponent } from './sucursales/cajas/caja/usuarios/caja-usuarios.component';
import { CajaDocumentosComponent } from './sucursales/cajas/caja/documentos/caja-documentos.component';
import { CajaEstadisticasComponent } from './sucursales/cajas/estadisticas/caja-estadisticas.component';


import { CategoriasComponent } from './categorias/categorias.component';
import { SubCategoriasComponent } from './categorias/subcategorias/subcategorias.component';
import { TiposComponent } from './categorias/tipos/tipos.component';

import { ReportesComponent } from './reportes/reportes.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    SharedModule,
    PipesModule,
    TagInputModule,
    AdminRoutingModule,
    FocusModule.forRoot(),
    PopoverModule.forRoot(),
    TabsModule.forRoot(),
    TooltipModule.forRoot(),
  ],
  declarations: [
    EmpresaComponent,
    DepartamentosComponent,
    CajasComponent,
    CajaComponent,
    CajaUsuariosComponent,
    CajaCortesComponent,
    CajaDocumentosComponent,
    CajaEstadisticasComponent,
    SucursalesComponent,
    SucursalComponent,
    SucursalUsuariosComponent,
    MesasComponent,
    BodegasComponent,
    CategoriasComponent,
    SubCategoriasComponent,
    TiposComponent,
    UsuariosComponent,
    UsuarioComponent,
    ReportesComponent
  ],
  exports: [
    EmpresaComponent,
    DepartamentosComponent,
    CajasComponent,
    CajaComponent,
    CajaUsuariosComponent,
    CajaCortesComponent,
    CajaDocumentosComponent,
    CajaEstadisticasComponent,
    SucursalesComponent,
    SucursalComponent,
    SucursalUsuariosComponent,
    MesasComponent,
    BodegasComponent,
    CategoriasComponent,
    SubCategoriasComponent,
    TiposComponent,
    UsuariosComponent,
    UsuarioComponent,
    ReportesComponent
  ]
})
export class AdminModule { }
