import { Component, OnInit, Input, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService, BsModalRef} from 'ngx-bootstrap/modal';

import { AlertService } from '../../../../services/alert.service';
import { ApiService } from '../../../../services/api.service';

@Component({
  selector: 'app-mesas',
  templateUrl: './mesas.component.html'
})
export class MesasComponent implements OnInit {

    @Input() sucursal:any = [];
    public mesa: any = {};
    public loading = false;
    public buscador:any = '';
    public estado:any = '';

    modalRef!: BsModalRef;

  	constructor( 
  	    private apiService: ApiService, private alertService: AlertService,
  	    private route: ActivatedRoute, private router: Router,
        private modalService: BsModalService
  	) { }

  	ngOnInit() {

  	}

    openModal(template: TemplateRef<any>, mesa:any) {
        this.mesa = mesa;
        if (!mesa.id ){
            this.mesa.numero = this.sucursal.mesas.length ? parseFloat(this.sucursal.mesas[this.sucursal.mesas.length - 1].numero) + 1 : 1;
            this.mesa.estado = 'Libre';
            this.mesa.sucursal_id = this.apiService.auth_user().sucursal_id;
        }
        this.modalRef = this.modalService.show(template);
    }
    
    public onSubmit() {
          this.loading = true;
          // Guardamos la mesa
          this.apiService.store('mesa', this.mesa).subscribe(mesa => {
              if (!this.mesa.id) {
                  this.sucursal.mesas.push(mesa);
              }
              this.mesa = {};
              this.alertService.success("Datos guardados");
              this.loading = false;
            this.modalRef?.hide();
          },error => {this.alertService.error(error); this.loading = false; });
    }

    public delete(id:number) {
        if (confirm('¿Desea eliminar el Registro?')) {
            this.apiService.delete('mesa/', id) .subscribe(data => {
                for (let i = 0; i < this.sucursal.mesas.length; i++) { 
                    if (this.sucursal.mesas[i].id == data.id )
                        this.sucursal.mesas.splice(i, 1);
                }
            }, error => {this.alertService.error(error); });
               
        }
    }

}
