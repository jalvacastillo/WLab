import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SucursalUsuariosComponent } from './sucursal-usuarios.component';

describe('SucursalUsuariosComponent', () => {
  let component: SucursalUsuariosComponent;
  let fixture: ComponentFixture<SucursalUsuariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SucursalUsuariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SucursalUsuariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
