import { Component, OnInit, Input, TemplateRef } from '@angular/core';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { AlertService } from '../../../../../services/alert.service';
import { ApiService } from '../../../../../services/api.service';

@Component({
  selector: 'app-sucursal-usuarios',
  templateUrl: './sucursal-usuarios.component.html'
})

export class SucursalUsuariosComponent implements OnInit {

	@Input() sucursal:any = [];
    public usuario:any = {};
    public cajas:any[] = [];
    public departamentos:any[] = [];
    public paginacion = [];
    public loading:boolean = false;
    public buscador:any = '';

    modalRef?: BsModalRef;

    constructor( public apiService:ApiService, private alertService:AlertService, private modalService: BsModalService ){}

	ngOnInit() {

    }

    openModal(template: TemplateRef<any>, usuario:any) {
        this.apiService.getAll('cajas').subscribe(cajas => { 
            this.cajas = cajas;
            this.loading = false;
        }, error => {this.alertService.error(error); });
        this.apiService.getAll('departamentos').subscribe(departamentos => { 
            this.departamentos = departamentos;
            this.loading = false;
        }, error => {this.alertService.error(error); });

        this.usuario = usuario;
        if (!this.usuario.id) {
            this.usuario.tipo = 'Mesero';
            this.usuario.sucursal_id = this.apiService.auth_user().sucursal_id;
            this.usuario.activo = true;
            this.usuario.empleado = true;
        }
        this.modalRef = this.modalService.show(template);
    }
    

    public onSubmit() {
        this.loading = true;
        // Guardamos al usuario
        this.apiService.store('usuario', this.usuario).subscribe(usuario => {
            if (!this.usuario.id) {
                this.sucursal.usuarios.push(usuario);
            }
            this.usuario = usuario;
            this.loading = false;
            this.alertService.success("Usuario guardado");
            this.modalRef?.hide();
        },error => {this.alertService.error(error); this.loading = false; });

    }

    public setEstado(usuario:any){
        this.apiService.store('usuario', usuario).subscribe(usuario => { 
            this.alertService.success('Actualizado');
        }, error => {this.alertService.error(error); });
    }

    public delete(id:number) {
        if (confirm('¿Desea eliminar el Registro?')) {
            this.apiService.delete('usuario/', id) .subscribe(data => {
                for (let i = 0; i < this.sucursal.usuarios.length; i++) { 
                    if (this.sucursal.usuarios[i].id == data.id )
                        this.sucursal.usuarios.splice(i, 1);
                }
            }, error => {this.alertService.error(error); });
                   
        }
    }


}

