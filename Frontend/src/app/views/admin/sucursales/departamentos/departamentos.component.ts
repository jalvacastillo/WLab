import { Component, OnInit, Input, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService, BsModalRef} from 'ngx-bootstrap/modal';

import { AlertService } from '../../../../services/alert.service';
import { ApiService } from '../../../../services/api.service';

@Component({
  selector: 'app-departamentos',
  templateUrl: './departamentos.component.html'
})
export class DepartamentosComponent implements OnInit {

    @Input() sucursal:any = [];
    public departamento: any = {};
    public categorias: any = [];
    public detalle: any = {};
    public loading = false;
    public buscador:any = '';
    public estado:any = '';

    modalRef!: BsModalRef;

  	constructor( 
  	    private apiService: ApiService, private alertService: AlertService,
  	    private route: ActivatedRoute, private router: Router,
        private modalService: BsModalService
  	) { }

  	ngOnInit() {

  	}

    openModal(template: TemplateRef<any>, departamento:any) {
        this.departamento = departamento;
        if (!departamento.id ){
            this.departamento.sucursal_id = this.apiService.auth_user().sucursal_id;
        }
        this.modalRef = this.modalService.show(template);
    }
    
    public onSubmit() {
          this.loading = true;
          // Guardamos la departamento
          this.apiService.store('departamento', this.departamento).subscribe(departamento => {
              if (!this.departamento.id) {
                  this.sucursal.departamentos.push(departamento);
              }
              this.departamento = {};
              this.alertService.success("Datos guardados");
              this.loading = false;
            this.modalRef?.hide();
          },error => {this.alertService.error(error); this.loading = false; });
    }

    public delete(id:number) {
        if (confirm('¿Desea eliminar el Registro?')) {
            this.apiService.delete('departamento/', id) .subscribe(data => {
                for (let i = 0; i < this.sucursal.departamentos.length; i++) { 
                    if (this.sucursal.departamentos[i].id == data.id )
                        this.sucursal.departamentos.splice(i, 1);
                }
            }, error => {this.alertService.error(error); });
               
        }
    }

// Detalles
    openModalDetalle(template: TemplateRef<any>, departamento:any) {
        this.departamento = departamento;
        this.loading = true;
        this.apiService.getAll('departamento/' + departamento.id).subscribe(departamento => {
            this.departamento = departamento;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});

        this.apiService.getAll('subcategorias').subscribe(categorias => {
            this.categorias = categorias;
            this.makeList();
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});

        this.modalRef = this.modalService.show(template, { class: 'modal-lg', backdrop: 'static' });
    }


    makeList(){
        for (var i = 0; i < this.categorias.length; ++i) {

            for (var j = 0; j < this.departamento.detalles.length; ++j) {
                if(this.departamento.detalles[j].categoria_id == this.categorias[i].id) {
                    this.categorias[i].checked = true;
                    this.categorias[i].detalle_id = this.departamento.detalles[j].id;
                }

            }
        }
        // console.log(this.categorias);
    }

    checked(detalle:any){
        console.log(detalle);
        if(detalle.checked) {
            this.onSubmitDetalle(detalle);
        }else{
            this.deleteDetalle(detalle.detalle_id);
        }

    }

    onSubmitDetalle(detalle:any):void{
        this.detalle.categoria_id = detalle.id;
        this.detalle.departamento_id = this.departamento.id;
        console.log(this.detalle);
        this.apiService.store('departamento/detalle', this.detalle).subscribe(detalle => {
            this.departamento.detalles.push(detalle);
            this.makeList();
        }, error => {this.alertService.error(error); this.loading = false;});

    }

    public deleteDetalle(id:number) {
        // if (confirm('¿Desea eliminar el Registro?')) {
            this.apiService.delete('departamento/detalle/', id) .subscribe(data => {
                for (let i = 0; i < this.departamento.detalles.length; i++) { 
                    if (this.departamento.detalles[i].id == data.id ){
                        this.departamento.detalles.splice(i, 1);
                        this.makeList();
                    }
                }
            }, error => {this.alertService.error(error); });
               
        // }
    }


}
