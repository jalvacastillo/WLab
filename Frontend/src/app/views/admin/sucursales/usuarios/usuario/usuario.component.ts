import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService } from '../../../../../services/alert.service';
import { ApiService } from '../../../../../services/api.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html'
})
export class UsuarioComponent implements OnInit {

	public usuario: any = {};
	public cajas: any = [];
	public sucursales: any = [];
	public departamentos: any = [];
  public loading = false;

  // Img Upload
    public file?:File;
    public preview = false;
    public url_img_preview:string = '';

	constructor( 
	    public apiService: ApiService, private alertService: AlertService,
	    private route: ActivatedRoute, private router: Router
	) { }

	ngOnInit() {
	    const id = +this.route.snapshot.paramMap.get('id')!;
	    
	    if(isNaN(id)){
	        this.usuario = {};
	        this.usuario.tipo = 'Mesero';
	        this.usuario.sucursal_id = this.apiService.auth_user().sucursal_id;
	        this.usuario.activo = true;
	        this.usuario.empleado = true;
	    }
	    else{
	        this.loadAll(id);
	    }

	    this.apiService.getAll('cajas').subscribe(cajas => { 
	        this.cajas = cajas;
	        this.loading = false;
	    }, error => {this.alertService.error(error); });

	    this.apiService.getAll('sucursales').subscribe(sucursales => { 
	        this.sucursales = sucursales;
	        this.loading = false;
	    }, error => {this.alertService.error(error); });

	    this.apiService.getAll('departamentos').subscribe(departamentos => { 
	        this.departamentos = departamentos;
	        this.loading = false;
	    }, error => {this.alertService.error(error); });
	}

	public loadAll(id:number) {
	    this.loading = true;

	    this.apiService.read('usuario/', id).subscribe(usuario => { 
	        this.usuario = usuario;
	        this.loading = false;
	    }, error => {this.alertService.error(error); this.loading = false;});


	}

	public onSubmit() {
	    this.loading = true;
	    if(this.usuario.tipo == 1) {
	    	this.usuario.caja_id == null;
	    }

	    let formData:FormData = new FormData();
	    for (var key in this.usuario) {
	        if (key == 'activo' || key == 'empleado') {
	            this.usuario[key] = this.usuario[key] ? 1 : 0;
	        }
	        formData.append(key, this.usuario[key] == null ? '' : this.usuario[key]);
	    }

	    // Guardamos al usuario
	    this.apiService.store('usuario', formData).subscribe(usuario => {
	    	this.usuario.avatar = usuario.avatar;
	    	let user:any = JSON.parse(localStorage.getItem('wanda_auth_user')!);
	    	user.avatar = usuario.avatar;
	    	localStorage.setItem('wanda_auth_user', JSON.stringify(user));

	        if (!this.usuario.id) {
		        this.router.navigate(['/admin/usuarios']);
	        }
	        this.usuario = usuario;
	        this.loading = false;
	        this.preview = false;
	        this.alertService.success("Usuario guardado");
	    },error => {this.alertService.error(error); this.loading = false; });

	}

	setFile(event:any){
	    this.file = event.target.files[0];
	    this.usuario.file = this.file;
	    var reader = new FileReader();
	    reader.onload = ()=> {
	        var url:any;
	        url = reader.result;
	        this.url_img_preview = url;
	        this.preview = true;
	       };
	    reader.readAsDataURL(this.file!);
	}

}
