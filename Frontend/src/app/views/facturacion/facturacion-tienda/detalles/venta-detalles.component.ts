import { Component, OnInit, EventEmitter, Input, Output, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { ApiService } from '../../../../services/api.service';
import { AlertService } from '../../../../services/alert.service';

@Component({
  selector: 'app-venta-detalles',
  templateUrl: './venta-detalles.component.html'
})
export class VentaDetallesComponent implements OnInit {

    @Input() venta: any = {};
    public detalle:any = {};
    public supervisor:any = {};
    public cantidad!:any;
    public precio!:any;

    @Output() update = new EventEmitter();
    @Output() sumTotal = new EventEmitter();
    modalRef!: BsModalRef;

    @ViewChild('msupervisor')
    public supervisorTemplate!: TemplateRef<any>;

    public buscador:string = '';
    public loading:boolean = false;

    constructor( 
        private apiService: ApiService, private alertService: AlertService,
        private modalService: BsModalService
    ) { }

    ngOnInit() {

    }

    openModalEdit(template: TemplateRef<any>, detalle:any) {
        this.detalle = detalle;
        this.cantidad = this.detalle.cantidad;
        this.precio = this.detalle.precio;
        this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
    }

    // Actualizar detalle
    actualizarDetalle(){
        this.detalle.total = (this.detalle.precio - this.detalle.descuento) * this.detalle.cantidad;
        
        if(this.detalle.tipo_impuesto == "Gravada") {
            this.detalle.iva     = (((this.detalle.precio - this.detalle.descuento) / 1.13) * this.detalle.cantidad) * 0.13;
        }
        
        this.detalle.total = ((this.detalle.precio - this.detalle.descuento) * this.detalle.cantidad);
        this.detalle.subcosto = (this.detalle.costo * this.detalle.cantidad);
        this.detalle.subtotal = (this.detalle.total - this.detalle.iva);

        // this.detalle = {};
        this.update.emit(this.venta);
    }

    public setCantidad(){
        this.detalle.cantidad = this.cantidad;
        this.detalle.precio = this.precio;
        this.actualizarDetalle()
        this.modalRef.hide();
    }

    public setPrecio(precio:any){
        this.precio = precio;
        this.actualizarDetalle();
    }

    public modalSupervisor(detalle:any){
        this.detalle = detalle;
        this.modalRef = this.modalService.show(this.supervisorTemplate, {class: 'modal-xs'});
    }

    public supervisorCheck(){
        this.loading = true;
        this.apiService.store('usuario-validar', this.supervisor).subscribe(supervisor => {
            this.modalRef.hide();
            this.eliminarDetalle(this.detalle);
            this.loading = false;
            this.supervisor = {};
        },error => {this.alertService.error(error); this.loading = false; });
    }

    // Eliminar detalle
        public eliminarDetalle(detalle:any){
            // if (confirm('Confirma que desea cerrar el turno en caja')) { 

            if(detalle.id) {
                this.apiService.delete('venta/detalle/', detalle.id).subscribe(detalle => {
                    for (var i = 0; i < this.venta.detalles.length; ++i) {
                        if (this.venta.detalles[i].producto_id === detalle.producto_id ){
                            this.venta.detalles.splice(i, 1);
                            this.update.emit(this.venta);
                        }
                    }
                },error => {this.alertService.error(error); this.loading = false; });
            }else{

                for (var i = 0; i < this.venta.detalles.length; ++i) {
                    if (this.venta.detalles[i].producto_id === detalle.producto_id ){
                        this.venta.detalles.splice(i, 1);
                        this.update.emit(this.venta);
                    }
                }
            }

        }

    public sumTotalEmit(){
        this.sumTotal.emit();
    }


}
