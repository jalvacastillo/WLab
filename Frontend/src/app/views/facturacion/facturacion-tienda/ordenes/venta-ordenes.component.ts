import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { ApiService } from '../../../../services/api.service';
import { AlertService } from '../../../../services/alert.service';

@Component({
  selector: 'app-venta-ordenes',
  templateUrl: './venta-ordenes.component.html'
})
export class VentaOrdenesComponent implements OnInit {

	modalRef!: BsModalRef;

    public ordenes:any = [];
    public loading = false;


	constructor( 
	    private apiService: ApiService, private alertService: AlertService,
	    private modalService: BsModalService,
	    private route: ActivatedRoute, private router: Router,
    ) {
        this.router.routeReuseStrategy.shouldReuseRoute = function() {return false; };
    }

	ngOnInit() {
    }
    
    openModalOrdenes(template: TemplateRef<any>) {
        this.loading = true;
        this.apiService.getAll('ventas/ordenes').subscribe(ordenes => { 
            this.ordenes = ordenes;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});
        this.modalRef = this.modalService.show(template);
    }
    
    selectOrden(orden:any){
        this.modalRef.hide();
        this.router.navigate(['facturacion/' + orden.id]);
    }

}
