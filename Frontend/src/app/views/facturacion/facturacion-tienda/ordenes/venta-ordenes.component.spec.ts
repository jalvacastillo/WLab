import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VentaOrdenesComponent } from './venta-ordenes.component';

describe('VentaOrdenesComponent', () => {
  let component: VentaOrdenesComponent;
  let fixture: ComponentFixture<VentaOrdenesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VentaOrdenesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VentaOrdenesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
