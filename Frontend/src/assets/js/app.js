$(document).ready(function() {
     $('.custom-file-input').on('change',function(){
        //get the file name
        var fileName = $(this).val();
        //replace the "Choose a file" label
        $(this).next('.custom-file-label').html(fileName);
    })
    $('.collapse').collapse();
   
   

    $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
      if (!$(this).next().hasClass('show')) {
        $(this).parents('.dropdown-menu').first().find('.show').removeClass('show');
      }
      var $subMenu = $(this).next('.dropdown-menu');
      $subMenu.toggleClass('show');


      $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
        $('.dropdown-submenu .show').removeClass('show');
      });


      return false;
    });
    
});


$(document).keydown(function(tecla){
   let letra;
   letra = '.tcla-' + tecla.key;
   $(letra).click();
 // console.log(letra);
    if (tecla.key == 'F5') {
        $('#lector').focus();
    }

   // Prevenir eventos de tecla F1 - F12
   if (tecla.keyCode <= 123 && tecla.keyCode >= 112) {
      tecla.preventDefault();
   }

});




$(document).on('click.bs.dropdown', '.dont-close-click', function (e) {
    e.stopPropagation();
});

$('[data-toggle="tooltip"]').tooltip();
